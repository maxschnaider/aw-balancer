import { useLogger } from '@aw-balancer/hooks';
import { useQuery } from 'react-query';
import { BalToken, BalTokenAmount, BalSwapInfo } from '@aw-balancer/types';

interface IUseBalQuoteSwap {
  tokenAmountIn?: BalTokenAmount | undefined;
  tokenOut?: BalToken | undefined;
}

export const useBalQuoteSwap = ({
  tokenAmountIn,
  tokenOut,
}: IUseBalQuoteSwap) => {
  const log = useLogger(useBalQuoteSwap.name);

  return useQuery(
    ['quote-balancer-swap', tokenAmountIn, tokenOut],
    async () => {
      if (!tokenAmountIn || !tokenAmountIn.amount || !tokenOut) return;
      const swapInfo: BalSwapInfo = {
        tokenAmountIn: tokenAmountIn,
        tokenAmountOut: new BalTokenAmount(tokenOut, tokenAmountIn.amount),
        minimumAmountOut: tokenAmountIn.amount || '0',
        rate: 1.0001,
        fee: 0,
        route: [],
      };
      return swapInfo;
    },
    {
      onError: (error) => log.error(error),
      onSuccess: (data) => log.success(data),
    }
  );
};
