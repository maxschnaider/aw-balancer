import { useLogger } from '@aw-balancer/hooks';
import _ from 'lodash';
import { useBalTokenPools } from '@aw-balancer/hooks';
import { BalPool, BalToken } from '@aw-balancer/types';
import { useQuery, UseQueryOptions } from 'react-query';

interface IUseBalTokensCommonPools extends Pick<UseQueryOptions, 'enabled'> {
  tokens: BalToken[];
}

export const useBalTokensCommonPools = ({
  tokens,
  enabled,
}: IUseBalTokensCommonPools) => {
  const log = useLogger(useBalTokensCommonPools.name);
  const { data: firstTokenPools } = useBalTokenPools({
    address: tokens[0]?.address,
  });

  return useQuery(
    ['fetch-balancer-tokens-common-pools', tokens, firstTokenPools],
    async () => {
      log.verbose('FETCH_TOKENS_COMMON_POOLS_QUERY', {
        tokens,
        firstTokenPools,
      });

      let commonTokensPools: BalPool[] = [];

      firstTokenPools?.filter((pool) => {
        tokens.forEach((token) => {
          if (
            pool.tokens?.some(
              (poolToken) =>
                poolToken.address.toLowerCase() === token.address.toLowerCase()
            )
          ) {
            commonTokensPools.push(pool);
          }
        });
      });

      commonTokensPools = _.uniqBy(commonTokensPools, 'id');

      return commonTokensPools;
    },
    {
      enabled,
      refetchOnWindowFocus: false,
      onError: (error) => log.error(error),
      onSuccess: (data) => log.success(data),
    }
  );
};
