import { BalToken } from '@aw-balancer/types';
import axios from 'axios';
import { useQuery } from 'react-query';
import {
  BALANCER_DEFAULT_CHAIN_ID,
  BALANCER_SUPPORTED_CHAIN_IDS,
  BALANCER_TOKEN_LIST_URI,
} from '@aw-balancer/constants';
import { useLogger } from '@aw-balancer/hooks';
import { useNetwork } from 'wagmi';

export type BalTokenListQueryResult = BalToken[];

type FetchBalTokenListData = {
  tokens: BalToken[];
};

export const useBalTokenList = () => {
  const log = useLogger(useBalTokenList.name);
  const { chain: activeChain } = useNetwork();

  return useQuery(
    ['fetch-balancer-token-list', activeChain],
    async (): Promise<BalTokenListQueryResult> => {
      const chainId =
        !!activeChain &&
        Object.values(BALANCER_SUPPORTED_CHAIN_IDS).includes(activeChain.id)
          ? activeChain.id
          : BALANCER_DEFAULT_CHAIN_ID;

      const {
        data: { tokens: tokensData },
      } = await axios.get<FetchBalTokenListData>(
        BALANCER_TOKEN_LIST_URI[chainId]
      );

      const tokens = tokensData
        .filter((token) => token.chainId === activeChain?.id)
        .map((token) => {
          token.address = token.address.toLowerCase();
          return token;
        });

      return tokens;
    },
    {
      refetchOnWindowFocus: false,
      onError: (error) => log.error(error),
      onSuccess: (data) => log.success(data),
    }
  );
};

// import { gql, QueryHookOptions, useQuery } from '@apollo/client'
// import { Token } from '@aw-balancer/types'
//
// export interface TokenListQueryResult {
//   tokens: Token[]
// }
//
// export interface IUseTokenList {
//   options?: QueryHookOptions
// }
//
// export const useTokenList = (args?: IUseTokenList) => {
//   const { options } = args || {}
//
//   return useQuery<TokenListQueryResult>(
//     gql`
//       query GetTokens {
//         tokens(
//           first: 100
//           orderDirection: desc
//           orderBy: totalBalanceUSD
//           where: { totalBalanceUSD_gt: "1000", symbol_not: null }
//         ) {
//           name
//           symbol
//           address
//           decimals
//         }
//       }
//     `,
//     options
//   )
// }
