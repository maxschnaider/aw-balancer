import { useLogger } from '@aw-balancer/hooks';
import { BalPool } from '@aw-balancer/types';
import axios from 'axios';
import { useQuery, UseQueryOptions } from 'react-query';
import {
  BALANCER_DEFAULT_CHAIN_ID,
  BALANCER_GRAPH_URL,
  BALANCER_SUPPORTED_CHAIN_IDS,
} from '@aw-balancer/constants';
import { useNetwork } from 'wagmi';

interface BalTokenPoolsGraphResponse {
  data: {
    poolTokens: {
      poolId: BalPool;
    }[];
  };
}

interface IUseBalTokenPools extends Pick<UseQueryOptions, 'enabled'> {
  address?: string;
}

type BalTokenPoolsQueryResult = Promise<BalPool[]>;

export const useBalTokenPools = ({ address, enabled }: IUseBalTokenPools) => {
  const log = useLogger(useBalTokenPools.name);
  const { chain: activeChain } = useNetwork();

  const tokenPoolsGraphQuery = `query {
  poolTokens(
    where: {address: "${address}", poolId_: {totalLiquidity_gt: "1000"}}
    orderBy: balance
    orderDirection: desc
    first: 1000
  ) {
    poolId {
      id
      address
      tokens {
        address
        symbol
        decimals
        name
        weight
      }
    }
  }
}`;

  return useQuery(
    ['fetch-balancer-token-pools', address, activeChain],
    async (): BalTokenPoolsQueryResult => {
      const chainId =
        !!activeChain &&
        Object.values(BALANCER_SUPPORTED_CHAIN_IDS).includes(activeChain.id)
          ? activeChain.id
          : BALANCER_DEFAULT_CHAIN_ID;

      const { data: graphResponseData } =
        await axios.post<BalTokenPoolsGraphResponse>(
          BALANCER_GRAPH_URL[chainId],
          {
            query: tokenPoolsGraphQuery,
          }
        );

      log.verbose('FETCH_TOKEN_POOLS_QUERY_DATA', {
        chain: activeChain,
        tokenPoolsGraphQuery,
        graphResponseData,
      });

      const tokenPools = graphResponseData.data.poolTokens.map(
        (token) => token.poolId
      );

      return tokenPools;
    },
    {
      enabled,
      refetchOnWindowFocus: false,
      onError: (error) => log.error(error),
      onSuccess: (data) => log.success(data),
    }
  );
};

// import { gql, useQuery } from '@apollo/client'
// import { constants } from 'ethers'
// export const useTokenPools = ({ address }: IUseTokenPools) => {
//   const log = useLogger(useTokenPools.name)

//   return useQuery<TokenPoolsQueryResult>(
//     gql`
//       ${tokenPoolsQuery}
//     `,
//     {
//       onError: (error) => log.error('TOKEN_POOLS_QUERY_ERROR', error),
//       variables: {
//         address: address || constants.AddressZero,
//       },
//     }
//   )
// }
