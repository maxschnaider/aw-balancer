import axios from 'axios';
import { useQuery, UseQueryOptions } from 'react-query';
import { useLogger } from '@aw-balancer/hooks';
import { utils } from 'ethers';
import { useNetwork, chain } from 'wagmi';
import { BALANCER_DEFAULT_CHAIN_ID } from '@aw-balancer/constants';

const COINGECKO_API_TOKEN_PRICE_URI =
  'https://api.coingecko.com/api/v3/simple/token_price';
const COINGECKO_CHAIN_IDS = {
  [chain.mainnet.id]: 'ethereum',
  [chain.polygon.id]: 'polygon-pos',
  [chain.arbitrum.id]: 'arbitrum-one',
};

interface CoingeckoApiTokenPriceQueryResponse {
  [address: string]:
    | {
        usd: number;
      }
    | undefined;
}

export type TokensPriceUsdQueryResponse = {
  [address: string]: number | undefined;
};

interface IUseTokensPriceUsd extends Pick<UseQueryOptions, 'enabled'> {
  addresses: string[];
}

export const useTokensPriceUsd = ({
  addresses,
  enabled,
}: IUseTokensPriceUsd) => {
  const log = useLogger(useTokensPriceUsd.name);
  const { chain } = useNetwork();

  return useQuery(
    ['fetch-tokens-price-usd', addresses, chain],
    async (): Promise<TokensPriceUsdQueryResponse> => {
      const coingeckoChainId =
        COINGECKO_CHAIN_IDS[chain?.id || BALANCER_DEFAULT_CHAIN_ID];
      const { data: fetchTokensPriceResultData } =
        await axios.get<CoingeckoApiTokenPriceQueryResponse>(
          `${COINGECKO_API_TOKEN_PRICE_URI}/${coingeckoChainId}`,
          {
            params: {
              contract_addresses: addresses.join(),
              vs_currencies: 'usd',
            },
          }
        );
      const tokenPriceUsdByAddress: TokensPriceUsdQueryResponse = {};
      addresses.map(
        (address) =>
          (tokenPriceUsdByAddress[address] =
            fetchTokensPriceResultData[utils.getAddress(address)]?.usd ||
            fetchTokensPriceResultData[address.toLowerCase()]?.usd)
      );
      return tokenPriceUsdByAddress;
    },
    {
      enabled,
      refetchOnWindowFocus: false,
      onSuccess: (data) => log.success(data),
      onError: (error) => log.error(error),
    }
  );
};
