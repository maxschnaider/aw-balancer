import { fetchBalance, FetchBalanceResult } from '@wagmi/core';
import { useQuery, UseQueryOptions } from 'react-query';
import { ERC20__factory } from '@aw-balancer/types/contracts';
import { useLogger, useSignerOrProvider } from '@aw-balancer/hooks';
import { BigNumber, constants } from 'ethers';
import { useAccount, useNetwork } from 'wagmi';

export interface TokenWithBalance extends FetchBalanceResult {
  address: string;
}

export interface UseTokenBalancesOptions
  extends Pick<UseQueryOptions, 'enabled'> {
  addresses: string[];
  account?: string | undefined;
}

export type TokenBalancesQueryResult = Promise<TokenWithBalance[]>;

export const useTokenBalances = ({
  addresses,
  account,
  enabled,
}: UseTokenBalancesOptions) => {
  const log = useLogger(useTokenBalances.name);
  const { chain } = useNetwork();
  const signerOrProvider = useSignerOrProvider();
  const { address } = useAccount();
  return useQuery(
    [`get-token-balances`, { addresses, account, chain }],
    (): TokenBalancesQueryResult => {
      return Promise.all(
        addresses.map(async (tokenAddress) => {
          const erc20 = ERC20__factory.connect(tokenAddress, signerOrProvider);
          try {
            await erc20.symbol(); // trycatch for shitcoins
            const decimals = await erc20.decimals();
            const chainId = chain?.id || 1;
            const balance = await fetchBalance({
              addressOrName: account || address || constants.AddressZero,
              chainId,
              token:
                tokenAddress === constants.AddressZero
                  ? undefined
                  : tokenAddress,
              formatUnits: decimals,
            });
            return { ...balance, address: tokenAddress };
          } catch {
            return {
              address: tokenAddress,
              decimals: 18,
              formatted: '0',
              symbol: '',
              unit: 18,
              value: BigNumber.from(0),
            };
          }
        })
      );
    },
    {
      enabled,
      cacheTime: 0,
      onSuccess: (data) => log.success('GET_TOKEN_BALANCES_SUCC', data),
      onError: (error) => log.error('GET_TOKEN_BALANCES_ERROR', error),
    }
  );
};
