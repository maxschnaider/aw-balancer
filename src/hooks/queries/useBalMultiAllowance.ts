import { utils } from 'ethers';
import { useLogger, useSignerOrProvider } from '@aw-balancer/hooks';
import { useQuery, UseQueryOptions } from 'react-query';
import { ERC20__factory } from '@aw-balancer/types/contracts';
import { useAccount } from 'wagmi';

export interface IUseBalMultiAllowance
  extends Pick<UseQueryOptions, 'enabled'> {
  tokens: (string | undefined)[];
  spender: string;
}

export interface UseMultiAllowanceQueryData {
  [address: string]: Number;
}

export const useBalMultiAllowance = ({
  tokens,
  spender,
  enabled,
}: IUseBalMultiAllowance) => {
  const log = useLogger(useBalMultiAllowance.name);
  const { address: account } = useAccount();
  const signerOrProvider = useSignerOrProvider();

  return useQuery(
    ['fetch-balncer-multi-allowance', tokens, account, spender],
    async (): Promise<UseMultiAllowanceQueryData> => {
      const allowanceByAddress: UseMultiAllowanceQueryData = {};
      if (!!account)
        await Promise.all(
          tokens.map(async (tokenAddress) => {
            if (!tokenAddress) return;
            const erc20 = ERC20__factory.connect(
              tokenAddress,
              signerOrProvider
            );
            const [allowance, decimals] = await Promise.all([
              erc20.allowance(account, spender),
              erc20.decimals(),
            ]);
            allowanceByAddress[tokenAddress] = Number(
              utils.formatUnits(allowance, decimals)
            );
          })
        );
      return allowanceByAddress;
    },
    {
      enabled,
      cacheTime: 0,
      onError: (error) => log.error(error),
      onSuccess: (data) => log.success(data),
    }
  );
};
