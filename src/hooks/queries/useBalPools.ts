import { gql, useQuery } from '@apollo/client';
import { useLogger } from '@aw-balancer/hooks';
import { BalPool } from '@aw-balancer/types';

export interface BalPoolsQueryResult {
  pools: BalPool[];
}

export interface IUseBalPools {
  limit: number;
}

const poolsQuery = `
    query GetPools($offset: Int, $limit: Int) {
        pools(
          offset: $offset
          limit: $limit
          where: { totalLiquidity_gt: 100 }
          orderBy: totalLiquidity
          orderDirection: desc
        ) {
          id
          address
          tokens {
            address
            symbol
            name
            decimals
            weight
          }
          totalLiquidity
        }
      }
    `;

export const useBalPools = ({ limit }: IUseBalPools) => {
  const log = useLogger(useBalPools.name);

  return useQuery<BalPoolsQueryResult>(
    gql`
      ${poolsQuery}
    `,
    {
      onCompleted: (data) => log.success(data),
      onError: (error) => log.error(error),
      variables: {
        offset: 0,
        limit,
      },
    }
  );
};
