import { utils } from 'ethers';
import { TokenWithBalance, useLogger } from '@aw-balancer/hooks';
import { BalPool } from '@aw-balancer/types';
import { useQuery } from 'react-query';
import { useAccount } from 'wagmi';
import { useBalPoolContract, useBalVaultContract } from '@aw-balancer/hooks';

export type BalPoolBalancesQueryResult = TokenWithBalance[] | undefined;

export interface IUseBalPoolBalances {
  pool?: BalPool | undefined;
}

export const useBalPoolBalances = ({ pool }: IUseBalPoolBalances) => {
  const log = useLogger(useBalPoolBalances.name);
  const { address } = useAccount();
  const poolContract = useBalPoolContract({ address: pool?.address });
  const vault = useBalVaultContract();

  return useQuery(
    ['fetch-balancer-pool-balances', pool?.address, address],
    async (): Promise<BalPoolBalancesQueryResult> => {
      if (!pool || !address) return;

      const tokensWithBalance: BalPoolBalancesQueryResult = [];

      const [myPoolShare, poolTotalSupply, { balances: poolTokensReserves }] =
        await Promise.all([
          poolContract.balanceOf(address),
          poolContract.totalSupply(),
          vault.getPoolTokens(pool.id),
        ]);

      await Promise.all([
        pool.tokens?.forEach(async (token, i) => {
          const balance = myPoolShare
            .mul(poolTokensReserves[i])
            .div(poolTotalSupply);

          const tokenWithBalance: TokenWithBalance = {
            ...token,
            value: balance,
            formatted: utils.formatUnits(balance, token.decimals),
          };
          tokensWithBalance.push(tokenWithBalance);
        }),
      ]);

      return tokensWithBalance;
    },
    {
      enabled: !!pool && !!address,
      cacheTime: 0,
      onSuccess: (data) => log.success(data),
      onError: (error) => log.error(error),
    }
  );
};
