import { constants } from 'ethers';
import { useQuery, UseQueryOptions } from 'react-query';
import { useErc20Contract } from '@aw-balancer/hooks';

export interface UseCheckAllowanceOptions
  extends Pick<UseQueryOptions, 'enabled'> {
  address: string | undefined;
  account: string | undefined;
  spender: string | undefined;
}

export const useCheckAllowance = ({
  address,
  account,
  spender,
  enabled,
}: UseCheckAllowanceOptions) => {
  const _address = address || constants.AddressZero;
  const _account = account || constants.AddressZero;
  const _spender = spender || constants.AddressZero;
  const contract = useErc20Contract(_address);
  return useQuery(
    ['check-allowance', { _address, _account, _spender }],
    () => {
      return contract.allowance(_account, _spender);
    },
    { enabled, cacheTime: 0 }
  );
};
