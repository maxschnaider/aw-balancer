import { gql, useQuery } from '@apollo/client';
import { useLogger } from '@aw-balancer/hooks';
import { BalPool } from '@aw-balancer/types';

export interface BalPoolQueryResult {
  pool?: BalPool;
}

export interface IUseBalPool {
  poolId: string;
}

export const useBalPool = ({ poolId }: IUseBalPool) => {
  const log = useLogger(useBalPool.name);

  return useQuery<BalPoolQueryResult>(
    gql`
      {
        pool(id: $poolId) {
          id
          address
          poolType
          factory
          strategyType
          symbol
          name
          swapEnabled
          swapFee
          owner
          totalWeight
          totalSwapFee
          totalSwapVolume
          totalLiquidity
          totalShares
          createTime
          swapsCount
          holdersCount
          tokens {
            id
            symbol
            name
            decimals
            address
          }
          tx
        }
      }
    `,
    {
      onCompleted: (data) => log.success(data),
      onError: (error) => log.error(error),
      variables: {
        poolId,
      },
    }
  );
};
