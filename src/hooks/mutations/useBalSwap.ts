import { useEstimateGas, useLogger } from '@aw-balancer/hooks';
import { useMutation } from 'react-query';
import { useBalVaultContract } from '@aw-balancer/hooks';

export const useBalSwap = () => {
  const log = useLogger(useBalSwap.name);
  const vault = useBalVaultContract();
  const { mutateAsync: estimateGas } = useEstimateGas();

  return useMutation('balancer-swap', async () => {}, {
    onSuccess: (data) => log.success(data),
    onError: (error) => log.error(error),
  });
};
