import { BigNumberish, ContractTransaction } from 'ethers';
import { useMutation } from 'react-query';
import { ERC20__factory } from '@aw-balancer/types/contracts';
import {
  useEstimateGas,
  useLogger,
  useSignerOrProvider,
} from '@aw-balancer/hooks';

export interface IUseApprove {
  token: string;
  onError?:
    | ((
        error: unknown,
        variables: {
          spender: string;
          value: BigNumberish;
        },
        context: unknown
      ) => void | Promise<unknown>)
    | undefined;
  onSuccess?:
    | ((
        data: ContractTransaction,
        variables: {
          spender: string;
          value: BigNumberish;
        },
        context: unknown
      ) => void | Promise<unknown>)
    | undefined;
  onSettled?:
    | ((
        data: ContractTransaction | undefined,
        error: unknown,
        variables: {
          spender: string;
          value: BigNumberish;
        },
        context: unknown
      ) => void | Promise<unknown>)
    | undefined;
  onMutate?:
    | ((variables: { spender: string; value: BigNumberish }) => unknown)
    | undefined;
}

interface IUseApproveMutate {
  spender: string;
  value: BigNumberish;
  token?: string;
}

export const useApprove = ({ token, ...options }: IUseApprove) => {
  const log = useLogger(useApprove.name);
  const signerOrProvider = useSignerOrProvider();
  const { mutateAsync: estimateGas } = useEstimateGas();

  return useMutation(
    ['approve-token'],
    async ({ spender, value, token: newToken }: IUseApproveMutate) => {
      log.verbose({ spender, value, token });
      const erc20 = ERC20__factory.connect(newToken || token, signerOrProvider);
      const { gasLimit, maxFeePerGas, maxPriorityFeePerGas } =
        await estimateGas({
          contract: erc20,
          methodName: 'approve',
          args: [spender, value],
        });
      return erc20.approve(spender, value, {
        gasLimit,
        // maxFeePerGas,
        // maxPriorityFeePerGas,
      });
    },
    {
      ...options,
    }
  );
};
