import { useMutation } from 'react-query';
import { useBalPoolContract, useBalVaultContract } from '@aw-balancer/hooks';
import { useAccount } from 'wagmi';
import { WeightedPoolEncoder } from '@balancer-labs/balancer-js';
import { useEstimateGas, useLogger } from '@aw-balancer/hooks';
import { BalPool, BalTokenAmount } from '@aw-balancer/types';
import { IVault } from '@aw-balancer/types/contracts/BalancerVault';

interface IUseBalWithdraw {
  pool?: BalPool | undefined;
  tokenAmounts?: BalTokenAmount[] | undefined;
}

interface ExitPoolArgs {
  poolId: string;
  sender: string;
  recipient: string;
  request: IVault.ExitPoolRequestStruct;
}

export const useBalWithdraw = ({ pool, tokenAmounts }: IUseBalWithdraw) => {
  const log = useLogger(useBalWithdraw.name);
  const { address } = useAccount();
  const vault = useBalVaultContract();
  const poolContract = useBalPoolContract({ address: pool?.address });
  const { mutateAsync: estimateGas } = useEstimateGas();

  return useMutation(
    'balancer-withdraw',
    async () => {
      log.verbose('mutate', pool, tokenAmounts);
      if (!pool || !tokenAmounts || !address) return;

      const tokens = tokenAmounts.map(
        (tokenAmount) => tokenAmount.token.address
      );
      const amounts = tokenAmounts.map(
        (tokenAmount) => tokenAmount.toBigNumber().mul(99).div(100) // 1% slippage
      );
      const bptAmount = await poolContract.balanceOf(address);

      const exitPoolArgs: ExitPoolArgs = {
        poolId: pool.id,
        sender: address,
        recipient: address,
        request: {
          assets: tokens,
          minAmountsOut: amounts,
          toInternalBalance: false,
          userData: WeightedPoolEncoder.exitExactBPTInForTokensOut(bptAmount),
        },
      };

      const { gasLimit, maxFeePerGas, maxPriorityFeePerGas } =
        await estimateGas({
          contract: vault,
          methodName: 'exitPool',
          args: Object.values(exitPoolArgs),
        });

      const tx = await vault.exitPool(
        exitPoolArgs.poolId,
        exitPoolArgs.sender,
        exitPoolArgs.recipient,
        exitPoolArgs.request,
        {
          gasLimit,
          // maxFeePerGas,
          // maxPriorityFeePerGas,
        }
      );

      return tx;
    },
    {
      onSuccess: (data) => log.success(data),
      onError: (error) => log.error(error),
    }
  );
};
