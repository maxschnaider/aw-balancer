import { useMutation } from 'react-query';
import { useBalVaultContract } from '@aw-balancer/hooks';
import { useAccount } from 'wagmi';
import { WeightedPoolEncoder } from '@balancer-labs/balancer-js';
import { useEstimateGas, useLogger } from '@aw-balancer/hooks';
import { BalPool, BalTokenAmount } from '@aw-balancer/types';
import { IVault } from '@aw-balancer/types/contracts/BalancerVault';

export interface IUseBalDeposit {
  pool?: BalPool | undefined;
  tokenAmounts?: BalTokenAmount[] | undefined;
}

interface ExitPoolArgs {
  poolId: string;
  sender: string;
  recipient: string;
  request: IVault.JoinPoolRequestStruct;
}

export const useBalDeposit = ({ pool, tokenAmounts }: IUseBalDeposit) => {
  const log = useLogger(useBalDeposit.name);
  const vault = useBalVaultContract();
  const { address } = useAccount();
  const { mutateAsync: estimateGas } = useEstimateGas();

  return useMutation(
    'balancer-deposit',
    async () => {
      log.verbose('mutate', pool, tokenAmounts);
      if (!pool || !tokenAmounts || !address) return;

      const tokens = tokenAmounts.map(
        (tokenAmount) => tokenAmount.token.address
      );
      const amounts = tokenAmounts.map((tokenAmount) =>
        tokenAmount.toBigNumber()
      );

      const joinPoolArgs: ExitPoolArgs = {
        poolId: pool.id,
        sender: address,
        recipient: address,
        request: {
          assets: tokens,
          maxAmountsIn: amounts,
          fromInternalBalance: false,
          userData: WeightedPoolEncoder.joinExactTokensInForBPTOut(amounts, 0),
        },
      };

      const { gasLimit, maxFeePerGas, maxPriorityFeePerGas } =
        await estimateGas({
          contract: vault,
          methodName: 'joinPool',
          args: Object.values(joinPoolArgs),
        });

      const tx = await vault.joinPool(
        joinPoolArgs.poolId,
        joinPoolArgs.sender,
        joinPoolArgs.recipient,
        joinPoolArgs.request,
        {
          gasLimit,
          // maxFeePerGas,
          // maxPriorityFeePerGas,
        }
      );

      return tx;
    },
    {
      onSuccess: (data) => log.success(data),
      onError: (error) => log.error(error),
    }
  );
};
