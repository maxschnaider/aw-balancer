import { Logger } from '@aw-balancer/utils';

export const useLogger = (name: string) => {
  return new Logger(name);
};
