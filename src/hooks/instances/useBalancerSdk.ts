import { useMemo } from 'react';
import { BalancerSDK, BalancerSdkConfig } from '@balancer-labs/sdk';
import {
  BALANCER_RPC_PROVIDERS,
  BALANCER_SUPPORTED_CHAIN_IDS,
} from '@aw-balancer/constants';
import { useNetwork } from 'wagmi';

export const useBalancerSdk = () => {
  const { chain } = useNetwork();

  return useMemo(() => {
    const chainId = chain?.id || BALANCER_SUPPORTED_CHAIN_IDS.Ethereum;
    const config: BalancerSdkConfig = {
      network: chainId,
      rpcUrl: BALANCER_RPC_PROVIDERS[chainId],
    };
    const balancer = new BalancerSDK(config);
    return balancer;
  }, [chain]);
};
