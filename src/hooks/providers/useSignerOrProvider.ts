import { useMemo } from 'react';
import { useNetwork, useProvider, useSigner } from 'wagmi';
import { BaseProvider } from '@ethersproject/providers';
import { Signer } from 'ethers';

export const useSignerOrProvider = (): Signer | BaseProvider => {
  const { data: signer } = useSigner();
  const { chain } = useNetwork();
  const provider = useProvider({ chainId: chain?.id });

  return useMemo(() => signer || provider, [signer, provider]);
};
