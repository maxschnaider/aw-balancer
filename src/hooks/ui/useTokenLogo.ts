import { utils } from 'ethers';
import { useMemo } from 'react';
import { useNetwork } from 'wagmi';

export interface IUseTokenLogo {
  address?: string | undefined;
}

export const useTokenLogo = ({ address }: IUseTokenLogo) => {
  const { chain } = useNetwork();

  return useMemo(
    () =>
      !!chain && !!address
        ? `https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/${chain.name.toLowerCase()}/assets/${utils.getAddress(
            address
          )}/logo.png`
        : '',
    [address, chain]
  );
};
