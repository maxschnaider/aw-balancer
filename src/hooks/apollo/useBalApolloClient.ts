import { ApolloClient, InMemoryCache } from '@apollo/client';
import { offsetLimitPagination } from '@apollo/client/utilities';
import {
  BALANCER_DEFAULT_CHAIN_ID,
  BALANCER_GRAPH_URL,
  BALANCER_SUPPORTED_CHAIN_IDS,
} from '@aw-balancer/constants';
import { useMemo } from 'react';
import { useNetwork } from 'wagmi';

export const useBalApolloClient = () => {
  const { chain } = useNetwork();
  const isSupportedChain = useMemo(
    () =>
      !!chain && Object.values(BALANCER_SUPPORTED_CHAIN_IDS).includes(chain.id),
    [chain]
  );

  return useMemo(
    () =>
      new ApolloClient({
        uri:
          !!chain && isSupportedChain
            ? BALANCER_GRAPH_URL[chain.id]
            : BALANCER_GRAPH_URL[BALANCER_DEFAULT_CHAIN_ID],
        cache: new InMemoryCache({
          typePolicies: {
            Query: {
              fields: {
                pools: {
                  ...offsetLimitPagination(),
                  read(existing, { args }) {
                    const { offset = 0, limit = existing?.length } = args ?? {};
                    return existing && existing.slice(offset, offset + limit);
                  },
                },
              },
            },
          },
        }),
      }),
    [isSupportedChain, chain]
  );
};
