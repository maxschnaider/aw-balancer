import { useMemo } from 'react';
import { BalancerPool__factory } from '@aw-balancer/types/contracts';
import { useSignerOrProvider } from '@aw-balancer/hooks';
import { constants } from 'ethers';

interface IUseBalPoolContract {
  address?: string | undefined;
}

export const useBalPoolContract = ({ address }: IUseBalPoolContract) => {
  const signerOrProvider = useSignerOrProvider();

  return useMemo(
    () =>
      BalancerPool__factory.connect(
        address || constants.AddressZero,
        signerOrProvider
      ),
    [address, signerOrProvider]
  );
};
