import { useMemo } from 'react';
import { BALANCER_VAULT_ADDRESS } from '@aw-balancer/constants';
import { BalancerVault__factory } from '@aw-balancer/types/contracts';
import { useSignerOrProvider } from '@aw-balancer/hooks';

export const useBalVaultContract = () => {
  const signerOrProvider = useSignerOrProvider();

  return useMemo(
    () =>
      BalancerVault__factory.connect(BALANCER_VAULT_ADDRESS, signerOrProvider),
    [signerOrProvider]
  );
};
