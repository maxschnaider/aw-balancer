import { useMemo } from 'react';
import { ERC20__factory } from '@aw-balancer/types/contracts';
import { useSignerOrProvider } from '@aw-balancer/hooks';

export const useErc20Contract = (address: string) => {
  const signerOrProvider = useSignerOrProvider();
  return useMemo(
    () => ERC20__factory.connect(address, signerOrProvider),
    [address, signerOrProvider]
  );
};
