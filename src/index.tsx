import React from 'react';
import ReactDOM from 'react-dom/client';
import BalancerFi from './BalancerFi';

ReactDOM.createRoot(document.getElementById('root')!).render(<BalancerFi />);
