import { extendTheme, theme as baseTheme } from "@chakra-ui/react";

const purpleColor = "#a855f7";
const darkBlueColor = "#2563eb";
const pinkColor = "#e811ec";
const blueColor = "#61a5fa";
const yellowColor = "#fed533";
const bgColor = "#10172a";
const bgLighterColor = "#162031";
const gradient = `linear-gradient(to right top, ${darkBlueColor}, ${pinkColor})`;
const modalOverlayGradient =
  "radial-gradient(circle at left,#ff0,transparent),radial-gradient(circle at bottom right,#00f,transparent),radial-gradient(circle at top,red,transparent)";

export const balancerTheme = extendTheme({
  colors: {
    balancer: {
      purple: purpleColor,
      pink: pinkColor,
      blue: blueColor,
      darkBlue: darkBlueColor,
      yellow: yellowColor,
      bg: bgColor,
      bgLighter: bgLighterColor,
      gradient,
      modalOverlayGradient,
    },
    baseTheme,
  },
});
