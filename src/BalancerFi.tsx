import { ApolloProvider } from '@apollo/client';
import { StackProps } from '@chakra-ui/react';
import { useEffect } from 'react';
import {
  useAccount,
  useConnect,
  useSigner,
  WagmiConfig,
  createClient,
  configureChains,
  chain,
} from 'wagmi';
import { publicProvider } from 'wagmi/providers/public';
import { InjectedConnector } from 'wagmi/connectors/injected';
import { jsonRpcProvider } from 'wagmi/providers/jsonRpc';
import { BalLayout } from '@aw-balancer/components';
import { useBalApolloClient } from '@aw-balancer/hooks';
import { BalancerFiRouterState } from '@aw-balancer/router/routes';
import { Router } from '@aw-balancer/router';
import { QueryClientProvider, QueryClient } from 'react-query';

export interface IBalancerFi extends Omit<StackProps, 'children'> {}

const BalancerFi = ({ ...props }: IBalancerFi) => {
  /**
   * apollo
   */
  const apolloClient = useBalApolloClient();

  /**
   * wagmi
   */
  const { chains, provider } = configureChains(
    [chain.mainnet, chain.polygon, chain.arbitrum],
    [
      publicProvider(),
      jsonRpcProvider({
        rpc: (chain) => {
          return { http: chain.rpcUrls.default };
        },
      }),
    ]
  );

  const wagmiClient = createClient({
    connectors: [new InjectedConnector({ chains })],
    provider,
  });

  /**
   * react query
   */
  const queryClient = new QueryClient();

  /**
   * reconnect
   */
  const { data: signer } = useSigner();
  const { address, isConnected } = useAccount();
  const { connectAsync, connectors } = useConnect();
  const [metamask] = connectors;
  useEffect(() => {
    if (!address || !signer || !isConnected)
      connectAsync({ connector: metamask });
  }, [isConnected, address, signer, metamask]);

  return (
    <WagmiConfig client={wagmiClient}>
      <QueryClientProvider client={queryClient}>
        <ApolloProvider client={apolloClient}>
          <BalLayout {...props}>
            <Router state={BalancerFiRouterState} />
          </BalLayout>
        </ApolloProvider>
      </QueryClientProvider>
    </WagmiConfig>
  );
};

export default BalancerFi;
