import { Box, BoxProps } from '@chakra-ui/react';
import { BALANCER_FI_BG_HEADER_SVG_URL } from '@aw-balancer/constants';
import { BalText } from '@aw-balancer/components';

export interface IBalHeader extends Omit<BoxProps, 'children'> {}

export const BalHeader = ({ ...props }: IBalHeader) => (
  <Box
    bgImg={BALANCER_FI_BG_HEADER_SVG_URL}
    // minH="275px"
    w="full"
    bgPos="50%"
    bgSize="cover"
    display="flex"
    justifyContent="center"
    alignItems="center"
    {...props}
  >
    <BalText
      color="white"
      fontSize="6xl"
      textAlign="center"
      fontWeight="bold"
      letterSpacing={0}
      p={4}
      textShadow="0px 3px 5px black"
      lineHeight="69px"
    >
      Automated portfolio manager and trading platform
    </BalText>
  </Box>
);
