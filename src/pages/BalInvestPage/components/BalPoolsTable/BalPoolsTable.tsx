import {
  Badge,
  HStack,
  Image,
  Skeleton,
  SkeletonCircle,
  Table,
  TableContainer,
  TableContainerProps,
  TableProps,
  Tbody,
  Thead,
  Tr,
  Wrap,
  WrapItem,
} from '@chakra-ui/react';
import { useLogger } from '@aw-balancer/hooks';
import { BalPool, BalToken } from '@aw-balancer/types';
import { useEffect, useState } from 'react';
import { BalButton, BalText, BalTh, BalTd } from '@aw-balancer/components';
import { useBalPools, useBalTokensCommonPools } from '@aw-balancer/hooks';
import { TokenAvatar } from '@aw-balancer/components';

export interface IBalPoolsTable {
  tableContainerProps?: TableContainerProps;
  tableProps?: TableProps;
  onPoolSelect: (pool: BalPool) => void;
  filterTokens: BalToken[];
}

export const BalPoolsTable = ({
  tableContainerProps,
  tableProps,
  onPoolSelect,
  filterTokens,
}: IBalPoolsTable) => {
  /**
   * logging
   */
  const log = useLogger(BalPoolsTable.name);

  /**
   * pools & pagination
   */
  const [pools, setPools] = useState<BalPool[]>([]);

  const poolsPerPagination = 10;

  const [poolsLimit, setPoolsLimit] = useState(poolsPerPagination);

  const {
    data: poolsQueryData,
    loading: poolsQueryLoading,
    fetchMore,
  } = useBalPools({ limit: poolsLimit });

  const { data: filteredPools, isLoading: filteredPoolsLoading } =
    useBalTokensCommonPools({
      tokens: filterTokens,
      enabled: filterTokens.length > 0,
    });

  useEffect(() => {
    log.verbose('POOLS', poolsQueryData, filteredPools);
    setPools(
      filterTokens.length > 0
        ? filteredPools || []
        : [...(poolsQueryData?.pools || [])]
    );
  }, [poolsQueryData, filteredPools]);

  const [morePoolsLoading, setMorePoolsLoading] = useState(false);

  const handleLoadMoreClick = () => {
    const currentLength = poolsQueryData?.pools.length || 0;
    setMorePoolsLoading(true);
    fetchMore({
      // variables: {
      // offset: currentLength,
      // limit: poolsPerPagination,
      // },
    }).then(() => {
      setPoolsLimit(currentLength + poolsPerPagination);
      setMorePoolsLoading(false);
    });
  };

  /**
   * UI
   */
  const renderLoadMoreTr = () => (
    <Tr
      onClick={handleLoadMoreClick}
      cursor="pointer"
      height="50px"
      position="relative"
    >
      <BalTd>
        <BalButton
          variant="ghost"
          isLoading={morePoolsLoading}
          position="absolute"
          top="5px"
          left="50%"
        >
          Load More
        </BalButton>
      </BalTd>
    </Tr>
  );

  const renderPoolTokensAvatars = (pool: BalPool) => (
    <HStack
      display="flex"
      pos="relative"
      w={`${25 + 4 * (pool.tokens?.length || 0)}px`}
      p={0}
    >
      {pool.tokens?.map((token, index) => (
        <TokenAvatar
          address={token.address}
          name={token.symbol}
          key={index}
          left={index === 0 ? 0 : index * 2 - 2}
          zIndex={index === 0 ? 99 : 99 - index}
          size="xs"
          borderColor="white"
          borderWidth="1px"
          pos="absolute"
        />
      ))}
    </HStack>
  );

  const renderPoolTokensBadges = (pool: BalPool) => (
    <Wrap spacing={1}>
      {pool.tokens?.map((token, index) => (
        <WrapItem key={index}>
          <Badge
            fontWeight="normal"
            borderRadius="5px"
            border={
              filterTokens.some(
                (filterToken) => filterToken.address === token.address
              )
                ? '2px solid #384aff'
                : '2px solid rgba(0,0,0,0)'
            }
          >
            <BalText fontSize="lg">
              {token.symbol}{' '}
              {!!token.weight && token.weight.substring(0, 4) + '%'}
            </BalText>
          </Badge>
        </WrapItem>
      ))}
    </Wrap>
  );

  const renderPoolList = () => (
    <>
      {pools.map((pool) => (
        <Tr
          key={pool.address}
          onClick={() => onPoolSelect(pool)}
          cursor="pointer"
          _hover={{ bgColor: 'blackAlpha.200' }}
        >
          <BalTd>{renderPoolTokensAvatars(pool)}</BalTd>
          <BalTd>{renderPoolTokensBadges(pool)}</BalTd>
          <BalTd>
            $
            {pool.totalLiquidity
              ?.split('.')[0]
              ?.replace(/\B(?=(\d{3})+(?!\d))/g, ',') || '-.--'}
          </BalTd>
          <BalTd>-</BalTd>
          <BalTd>-</BalTd>
        </Tr>
      ))}
    </>
  );

  const renderPoolListSkeleton = () => (
    <>
      {Array.from({ length: 10 }).map((_, index) => (
        <Tr h="50px" w="full" key={index}>
          {Array.from({ length: 5 }).map((_, index) => (
            <BalTd key={index}>
              {index === 0 ? (
                <HStack display="flex" pos="relative">
                  {Array.from({ length: 4 }).map((_, index) => (
                    <SkeletonCircle
                      w="25px"
                      h="25px"
                      key={index}
                      left={index === 0 ? 0 : index * 2 - 2}
                      zIndex={index === 0 ? 99 : 99 - index}
                      pos="absolute"
                    />
                  ))}
                </HStack>
              ) : (
                <Skeleton h="25px" w="full" />
              )}
            </BalTd>
          ))}
        </Tr>
      ))}
    </>
  );

  return (
    <TableContainer bg="balancer.bgLighter" {...tableContainerProps}>
      <Table variant="simple" colorScheme="blackAlpha" {...tableProps}>
        <Thead>
          <Tr>
            <BalTh>
              <Image
                src="https://polygon.balancer.fi/img/tokens_white.0c2dd4f3.svg"
                opacity="0.7"
              />
            </BalTh>
            <BalTh>Composition</BalTh>
            <BalTh>Pool Value</BalTh>
            <BalTh>Volume (24h)</BalTh>
            <BalTh>APR</BalTh>
          </Tr>
        </Thead>
        <Tbody>
          {poolsQueryLoading || filteredPoolsLoading
            ? renderPoolListSkeleton()
            : renderPoolList()}
          {filterTokens.length === 0 && renderLoadMoreTr()}
        </Tbody>
      </Table>
    </TableContainer>
  );
};
