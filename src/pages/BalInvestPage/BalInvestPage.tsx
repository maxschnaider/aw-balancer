import { HStack, StackItem, useDisclosure, VStack } from '@chakra-ui/react';
import { useLogger } from '@aw-balancer/hooks';
import {
  BalButton,
  BalText,
  BalTokensModal,
  TokenAvatar,
} from '@aw-balancer/components';
import {
  BalHeader,
  BalPoolsTable,
} from '@aw-balancer/pages/BalInvestPage/components';
import { useState } from 'react';
import { BalToken } from '@aw-balancer/types';
import { MdSearch } from 'react-icons/md';
import { useBalRouter, BalancerFiView } from '@aw-balancer/router';

export const BalInvestPage = () => {
  /**
   * logging
   */
  const log = useLogger(BalInvestPage.name);

  /**
   * routing
   */
  const [, { navigate }] = useBalRouter();

  /**
   * filtering
   */
  const [filterTokens, setFilterTokens] = useState<BalToken[]>([]);

  /**
   * UI
   */
  const renderPoolListTable = () => (
    <BalPoolsTable
      onPoolSelect={(pool) => {
        log.verbose('POOL', pool);
        navigate({ path: BalancerFiView.Pool, params: { pool } });
      }}
      filterTokens={filterTokens}
    />
  );

  const renderFilterButton = () => (
    <BalButton
      leftIcon={<MdSearch size="22px" opacity="0.5" />}
      bg="balancer.bgLighter"
      _hover={{ bg: 'balancer.gradient' }}
      _active={{ bg: 'balancer.gradient' }}
      onClick={openTokensModal}
    >
      Filter by token
    </BalButton>
  );

  const renderFilterToken = (token: BalToken) => (
    <HStack w="full">
      <StackItem>
        <TokenAvatar
          logoURI={token.logoURI}
          address={token.address}
          name={token.symbol}
          size="xs"
        />
      </StackItem>
      <StackItem>
        <BalText>{token.symbol}</BalText>
      </StackItem>
      <StackItem>
        <BalButton
          onClick={() =>
            setFilterTokens(
              filterTokens.filter((token) => token.address !== token.address)
            )
          }
        >
          x
        </BalButton>
      </StackItem>
    </HStack>
  );

  const renderFilterTokens = () => (
    <HStack spacing={4} justifyContent="end">
      <StackItem />
      {filterTokens.map((token) => (
        <StackItem key={token.address}>{renderFilterToken(token)}</StackItem>
      ))}
      <StackItem />
    </HStack>
  );

  const renderFilterPanel = () => (
    <VStack py={4}>
      <StackItem w="full" px={4}>
        <HStack justifyContent="space-between">
          <StackItem>
            <BalText fontSize="3xl">Investment pools</BalText>
          </StackItem>
          <StackItem>{renderFilterButton()}</StackItem>
        </HStack>
      </StackItem>
      {filterTokens.length > 0 && (
        <StackItem w="full" overflow="auto">
          {renderFilterTokens()}
        </StackItem>
      )}
    </VStack>
  );

  const {
    isOpen: isTokensModalOpen,
    onOpen: openTokensModal,
    onClose: closeTokensModal,
  } = useDisclosure();

  const renderTokensModal = () => (
    <BalTokensModal
      isOpen={isTokensModalOpen}
      onClose={closeTokensModal}
      onTokenSelect={(token) => {
        setFilterTokens([...filterTokens, token]);
        closeTokensModal();
      }}
    />
  );

  return (
    <>
      <BalHeader />
      <VStack spacing={0}>
        <StackItem w="full">{renderFilterPanel()}</StackItem>
        <StackItem w="full">{renderPoolListTable()}</StackItem>
      </VStack>
      {renderTokensModal()}
    </>
  );
};
