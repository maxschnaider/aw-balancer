import {
  TokenWithBalance,
  useApprove,
  useLogger,
  useTokenBalances,
  useTokensPriceUsd,
} from '@aw-balancer/hooks';
import { useEffect, useMemo, useState } from 'react';
import {
  StackProps,
  Grid,
  GridItem,
  StackItem,
  useDisclosure,
  VStack,
} from '@chakra-ui/react';
import { BalButton } from '@aw-balancer/components';
import {
  BalState,
  BalPool,
  BalTokenAmount,
  BalInvestType,
} from '@aw-balancer/types';
import {
  BalInvestConfirmModal,
  BalBalancesCard,
} from '@aw-balancer/pages/BalPoolPage/components';
import { constants } from 'ethers';
import {
  BALANCER_SUPPORTED_CHAIN_IDS,
  BALANCER_VAULT_ADDRESS,
} from '@aw-balancer/constants';
import {
  useBalDeposit,
  useBalMultiAllowance,
  useBalPoolBalances,
  useBalWithdraw,
} from '@aw-balancer/hooks';
import { useNetwork, useSigner, useWaitForTransaction } from 'wagmi';
import { useToaster } from '@aw-balancer/hooks';
import { BalInvestCard } from './components/BalInvestCard/BalInvestCard';
import { useBalRouter } from '@aw-balancer/router';

export interface IBalInvestView extends Omit<StackProps, 'children'> {}

export const BalPoolPage = ({ ...props }: IBalInvestView) => {
  /**
   * init
   */
  const log = useLogger(BalPoolPage.name);
  const toaster = useToaster();

  /**
   * routing
   */
  const [{ params }, { back }] = useBalRouter();

  /**
   * pool
   */
  const pool: BalPool | undefined = useMemo(() => params?.pool, [params]);

  /**
   * invest type
   */
  const [investType, setInvestType] = useState<BalInvestType>('deposit');

  const switchInvestType = () => {
    setInvestType(investType === 'deposit' ? 'withdraw' : 'deposit');
    resetTokenAmounts();
  };

  /**
   * web3
   */
  const { data: signer } = useSigner();
  const { chain: activeChain } = useNetwork();

  const isSupportedChain = useMemo(
    () =>
      !!activeChain &&
      Object.values(BALANCER_SUPPORTED_CHAIN_IDS).includes(activeChain.id),
    [activeChain]
  );

  /**
   * balances
   */
  const { data: walletBalances, refetch: refetchWalletBalances } =
    useTokenBalances({
      addresses: pool?.tokens?.map((token) => token.address) || [],
    });

  const { data: poolBalances, refetch: refetchPoolBalances } =
    useBalPoolBalances({
      pool,
    });

  const { data: tokenPriceUsdByAddress } = useTokensPriceUsd({
    addresses: pool?.tokens?.map((token) => token.address) || [],
  });

  const refetchBalances = () => {
    refetchPoolBalances();
    refetchWalletBalances();
  };

  /**
   * amounts
   */
  const [tokenAmounts, setTokenAmounts] = useState<BalTokenAmount[]>(
    pool?.tokens?.map((token) => new BalTokenAmount(token)) || []
  );
  const [totalAmountUsd, setTotalAmountUsd] = useState(0);

  useEffect(() => {
    if (!tokenPriceUsdByAddress) return;
    let totalAmountUsd = 0;
    tokenAmounts.forEach((tokenAmount) => {
      const tokenBalanceUsd =
        Number(tokenAmount.amount || 0) *
        (tokenPriceUsdByAddress[tokenAmount.token.address] || 0);
      totalAmountUsd += tokenBalanceUsd;
    });
    setTotalAmountUsd(totalAmountUsd);
    log.verbose('CALC_TOTAL_AMOUNT_USD', tokenAmounts, totalAmountUsd);
  }, [tokenAmounts, tokenPriceUsdByAddress]);

  const resetTokenAmounts = () => {
    log.verbose('resetTokenAmounts');
    setTokenAmounts(
      pool?.tokens?.map((token) => new BalTokenAmount(token)) || []
    );
  };

  /**
   * allowance
   */
  const { data: tokenAllowanceByAddress, refetch: checkAllowance } =
    useBalMultiAllowance({
      tokens: pool?.tokens?.map((token) => token.address) || [],
      spender: BALANCER_VAULT_ADDRESS,
    });

  const [tokenAmountsToApprove, setTokenAmountsToApprove] = useState<
    BalTokenAmount[]
  >([]);

  const [currentTokenAmountToApprove, setCurrentTokenAmountToApprove] =
    useState<BalTokenAmount | undefined>();

  useEffect(() => {
    if (!tokenAllowanceByAddress) return;
    const tokenAmountsToApprove: BalTokenAmount[] = [];
    Object.keys(tokenAllowanceByAddress).forEach((address) => {
      const tokenAmount = tokenAmounts.find(
        (tokenAmount) => tokenAmount.token.address === address
      );
      if (
        !!tokenAmount &&
        tokenAllowanceByAddress[address] < Number(tokenAmount.amount)
      ) {
        tokenAmountsToApprove.push(tokenAmount);
      }
    });
    setTokenAmountsToApprove(tokenAmountsToApprove);
    log.verbose('setTokenAmountsToApprove()', tokenAmountsToApprove);
  }, [tokenAllowanceByAddress, tokenAmounts]);

  useEffect(
    () => setCurrentTokenAmountToApprove(tokenAmountsToApprove[0]),
    [tokenAmountsToApprove]
  );

  const {
    data: approveTx,
    isLoading: isSigningApprove,
    mutate: approve,
  } = useApprove({
    token: currentTokenAmountToApprove?.token.address || constants.AddressZero,
  });

  const { isLoading: isLoadingApprove } = useWaitForTransaction({
    hash: approveTx?.hash,
    confirmations: 10,
    onSuccess: (approveReceipt) => {
      log.verbose('APPROVE_TX_SUCC', approveReceipt);
      toaster.showApproveSuccessToast();
      checkAllowance();
    },
    onError: (error) => {
      log.error('APPROVE_TX_ERROR', error);
      toaster.showApproveErrorToast();
    },
  });

  const handleApprove = () => {
    if (!currentTokenAmountToApprove) return;
    log.verbose('HANDLE_APPORVE', currentTokenAmountToApprove);
    approve({
      spender: BALANCER_VAULT_ADDRESS,
      value: currentTokenAmountToApprove.toBigNumber(),
    });
  };

  /**
   * deposit
   */
  const {
    data: depositTx,
    isLoading: isSigningDeposit,
    reset: resetDepositTx,
    mutate: deposit,
  } = useBalDeposit({ pool, tokenAmounts });

  const { data: depositReceipt, isLoading: isLoadingDeposit } =
    useWaitForTransaction({
      hash: depositTx?.hash,
      confirmations: 7,
      onSuccess: (depositReceipt) => {
        if (depositReceipt.status === 1) {
          log.success('DEPOSIT_TX_SUCC', depositReceipt);
          toaster.showDepositSuccessToast();
        } else {
          log.error('DEPOSIT_TX_ERROR', depositReceipt);
          toaster.showDepositErrorToast();
        }
      },
      onError: (error) => {
        log.error('DEPOSIT_TX_ERROR', error);
        toaster.showDepositErrorToast();
      },
    });

  /**
   * withdraw
   */
  const {
    data: withdrawTx,
    isLoading: isSigningWithdraw,
    reset: resetWithdrawTx,
    mutate: withdraw,
  } = useBalWithdraw({ pool, tokenAmounts });

  const { data: withdrawReceipt, isLoading: isLoadingWithdraw } =
    useWaitForTransaction({
      hash: withdrawTx?.hash,
      confirmations: 7,
      onSuccess: (withdrawReceipt) => {
        if (withdrawReceipt.status === 1) {
          log.success('WITHDRAW_TX_SUCC', withdrawReceipt);
          toaster.showWithdrawSuccessToast();
        } else {
          log.error('WITHDRAW_TX_ERROR', withdrawReceipt);
          toaster.showWithdrawErrorToast();
        }
      },
      onError: (error) => {
        log.error('WITHDRAW_TX_ERROR', error);
        toaster.showWithdrawErrorToast();
      },
    });

  const resetInvestTx = () => {
    resetDepositTx();
    resetWithdrawTx();
  };

  useEffect(() => {
    refetchBalances();
  }, [depositReceipt, withdrawReceipt]);

  /**
   * state
   */
  const currentState = useMemo(() => {
    if (!signer) {
      return BalState.NoSigner;
    }
    if (!isSupportedChain) {
      return BalState.WrongNetwork;
    }
    if (totalAmountUsd === 0) {
      return BalState.InvalidAmount;
    }
    if (
      isSigningApprove ||
      isLoadingApprove ||
      isSigningDeposit ||
      isLoadingDeposit ||
      isSigningWithdraw ||
      isLoadingWithdraw
    ) {
      return BalState.Loading;
    }
    if (tokenAmountsToApprove.length > 0 && investType === 'deposit') {
      return BalState.ApproveRequired;
    }
    if (depositTx || withdrawTx) {
      return BalState.Submitted;
    }
    return BalState.Ready;
  }, [
    signer,
    isSupportedChain,
    totalAmountUsd,
    isSigningApprove,
    isLoadingApprove,
    tokenAmountsToApprove.length,
    investType,
    isSigningDeposit,
    isLoadingDeposit,
    isSigningWithdraw,
    isLoadingWithdraw,
    depositTx,
    withdrawTx,
  ]);

  const resetState = () => {
    resetInvestTx();
    resetTokenAmounts();
    refetchBalances();
  };

  /**
   * handles
   */
  const handleActionButtonClick = () => {
    if (currentState === BalState.ApproveRequired) {
      return handleApprove();
    }
    if (investType === 'deposit') {
      deposit();
    } else {
      withdraw();
    }
  };

  /**
   * UI
   */
  const {
    onOpen: openInvestPreviewModal,
    onClose: closeInvestPreviewModal,
    isOpen: isOpenInvestPreviewModal,
  } = useDisclosure();

  const renderInvestCard = () => (
    <BalInvestCard
      investType={investType}
      tokenAmounts={tokenAmounts}
      totalAmountUsd={totalAmountUsd}
      walletBalances={walletBalances}
      poolBalances={poolBalances}
      tokenPriceUsdByAddress={tokenPriceUsdByAddress}
      handlePreviewButtonClick={openInvestPreviewModal}
      setTokenAmounts={setTokenAmounts}
    />
  );

  const renderSwitchInvestTypeButton = () => (
    <BalButton
      bg="balancer.gradient"
      _hover={{ bg: '' }}
      _focus={{ bg: '' }}
      color="white"
      size="lg"
      w="full"
      onClick={switchInvestType}
    >
      {investType === 'deposit' ? 'Withdraw' : 'Invest'}
    </BalButton>
  );

  const renderBalancesCard = (
    header: string,
    balances?: TokenWithBalance[] | undefined
  ) => (
    <VStack>
      <StackItem w="full">
        <BalBalancesCard
          minW="270px"
          headerText={header}
          tokens={pool?.tokens || []}
          balances={balances}
          tokenPriceUsdByAddress={tokenPriceUsdByAddress}
        />
      </StackItem>
      <StackItem w="full">
        {investType === 'withdraw' &&
          balances?.some((balance) => Number(balance.formatted) > 0) &&
          renderSwitchInvestTypeButton()}
      </StackItem>
    </VStack>
  );

  const renderBalancesCards = () => (
    <Grid templateColumns="repeat(2, 1fr)" gap={[0, 4]}>
      <GridItem colSpan={[2, 1]}>
        {renderBalancesCard('Pool tokens in my wallet', walletBalances)}
      </GridItem>
      <GridItem colSpan={[2, 1]}>
        {renderBalancesCard('My pool balance', poolBalances)}
      </GridItem>
    </Grid>
  );

  const renderInvestConfirmModal = () => (
    <BalInvestConfirmModal
      investType={investType}
      state={currentState}
      tokenAmounts={tokenAmounts}
      tokensToApprove={tokenAmountsToApprove.map(
        (tokenAmount) => tokenAmount.token
      )}
      totalAmountUsd={totalAmountUsd}
      tokenPriceUsdByAddress={tokenPriceUsdByAddress}
      onConfirmButtonClick={handleActionButtonClick}
      receipt={depositReceipt || withdrawReceipt}
      isOpen={isOpenInvestPreviewModal}
      onClose={() => {
        closeInvestPreviewModal();
        resetState();
      }}
    />
  );

  const renderBackButton = () => (
    <BalButton
      onClick={back}
      _hover={{ bg: 'balancer.gradient' }}
      _active={{ bg: 'balancer.gradient' }}
    >
      Back
    </BalButton>
  );

  return (
    <>
      <VStack spacing={4} p={4} alignItems="start" {...props}>
        <StackItem>{renderBackButton()}</StackItem>
        <StackItem w="full">{renderInvestCard()}</StackItem>
        <StackItem w="full">{renderBalancesCards()}</StackItem>
      </VStack>
      {renderInvestConfirmModal()}
    </>
  );
};
