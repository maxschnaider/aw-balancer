import {
  Divider,
  Flex,
  HStack,
  Link,
  StackDivider,
  StackItem,
  VStack,
} from '@chakra-ui/react';
import {
  BalState,
  BalInvestType,
  BalToken,
  BalTokenAmount,
} from '@aw-balancer/types';
import {
  BalButton,
  BalText,
  BalModal,
  IBalModal,
} from '@aw-balancer/components';
import { TokenAvatar } from '@aw-balancer/components';
import { useEffect, useState } from 'react';
import { TokensPriceUsdQueryResponse } from '@aw-balancer/hooks';
import { ContractReceipt } from 'ethers';
import ReactCanvasConfetti from 'react-canvas-confetti';
import { useConfetti } from '@aw-balancer/hooks';
import { useNetwork } from 'wagmi';
import { BALANCER_DEFAULT_CHAIN_ID, EXPLORERS } from '@aw-balancer/constants';

export interface IBalInvestConfirmModal
  extends Omit<IBalModal, 'title' | 'children'> {
  investType: BalInvestType;
  state: BalState;
  tokenAmounts: BalTokenAmount[];
  tokensToApprove: BalToken[];
  tokenPriceUsdByAddress?: TokensPriceUsdQueryResponse | undefined;
  totalAmountUsd: number;
  onConfirmButtonClick: () => void;
  receipt?: ContractReceipt | undefined;
}

export const BalInvestConfirmModal = ({
  investType,
  state,
  tokenAmounts,
  tokensToApprove,
  totalAmountUsd,
  tokenPriceUsdByAddress,
  onConfirmButtonClick,
  receipt,
  isOpen,
  onClose,
  ...props
}: IBalInvestConfirmModal) => {
  /**
   * web3
   */
  const { chain } = useNetwork();

  /**
   * steps logic
   */
  const [steps, setSteps] = useState(1);
  const [currentStep, setCurrentStep] = useState(0);

  useEffect(() => {
    setSteps(1);
    setCurrentStep(0);
  }, [isOpen]);

  useEffect(() => {
    if (tokensToApprove.length + 1 <= steps) {
      const _currentStep = Math.abs(tokensToApprove.length + 1 - steps);
      setCurrentStep(_currentStep);
    } else {
      let _steps = 1;
      tokensToApprove.forEach(() => {
        _steps += 1;
      });
      setSteps(_steps);
    }
  }, [steps, tokensToApprove]);

  const isRenderSteps =
    state !== BalState.Submitted && investType === 'deposit' && steps > 1;

  /**
   * confetti
   */
  const { getInstance, canvasStyles, startAnimation, stopAnimation } =
    useConfetti();

  useEffect(() => {
    if (state === BalState.Submitted) {
      startAnimation();
      setTimeout(stopAnimation, 6000);
    }
  }, [state]);

  /**
   * UI
   */
  const renderTokenAmounts = () => (
    <VStack
      borderWidth="1px"
      divider={<StackDivider />}
      borderRadius="lg"
      borderColor="blackAlpha.500"
      spacing={0}
    >
      {tokenAmounts.map((tokenAmount) => {
        const tokenPriceUsd =
          tokenPriceUsdByAddress?.[tokenAmount.token.address];
        const tokenAmountUsd =
          Number(tokenAmount.amount || 0) * (tokenPriceUsd || 0);
        return (
          <StackItem key={tokenAmount.token.address} w="full" px={3} py={2}>
            <HStack justifyContent="space-between" alignItems="center">
              <StackItem mr={2}>
                <TokenAvatar
                  logoURI={tokenAmount.token.logoURI}
                  address={tokenAmount.token.address}
                  name={tokenAmount.token.symbol}
                  size="sm"
                />
              </StackItem>
              <StackItem>
                <BalText>
                  {Number(tokenAmount.amount) > 0
                    ? tokenAmount.amount?.substring(0, 6)
                    : '0.0'}
                </BalText>
              </StackItem>
              <StackItem flex={1}>
                <BalText>{tokenAmount.token.symbol}</BalText>
              </StackItem>
              <StackItem>
                <BalText opacity="0.5">
                  <>${tokenAmountUsd?.toFixed(2) || '0.00'}</>
                </BalText>
              </StackItem>
            </HStack>
          </StackItem>
        );
      })}
    </VStack>
  );

  const renderSummary = () => (
    <VStack
      borderWidth="1px"
      borderRadius="lg"
      borderColor="blackAlpha.500"
      px={3}
      py={1}
      spacing={0}
    >
      <StackItem w="full">
        <BalText fontSize="2xl">Summary</BalText>
      </StackItem>
      <StackItem display="flex" justifyContent="space-between" w="full">
        <BalText>Total</BalText>
        <BalText>${totalAmountUsd.toFixed(2)}</BalText>
      </StackItem>
      <StackItem display="flex" justifyContent="space-between" w="full">
        <BalText>Price Impact</BalText>
        <BalText>{'< 0,00%'}</BalText>
      </StackItem>
    </VStack>
  );
  const renderSteps = () => (
    <HStack spacing={0} justifyContent="center">
      {new Array(steps).fill(0).map((_, index) => (
        <>
          <StackItem key={index * 2}>
            <Flex
              w="25px"
              h="25px"
              borderRadius="full"
              justifyContent="center"
              alignItems="center"
              border="1px solid"
              borderColor={
                currentStep > index
                  ? 'green.400'
                  : currentStep === index
                  ? 'balancer.purple'
                  : 'whiteAlpha.300'
              }
              color={
                currentStep > index
                  ? 'green.400'
                  : currentStep === index
                  ? 'balancer.purple'
                  : 'white'
              }
              fontSize="xl"
            >
              {currentStep > index ? '✔' : index + 1}
            </Flex>
          </StackItem>
          {index < steps - 1 && (
            <StackItem key={index * 2 + 1}>
              <Divider w="20px" bg="whiteAlpha.300" />
            </StackItem>
          )}
        </>
      ))}
    </HStack>
  );

  const renderConfirmButton = () => (
    <BalButton
      bg="balancer.gradient"
      _hover={{ bg: '' }}
      _active={{ bg: '' }}
      color="white"
      size="lg"
      w="full"
      onClick={!receipt ? onConfirmButtonClick : onClose}
      isLoading={state === BalState.Loading}
    >
      {state === BalState.Submitted
        ? 'Return to pool page'
        : state === BalState.ApproveRequired
        ? `Approve ${tokensToApprove[0]?.symbol} for Investing`
        : investType === 'deposit'
        ? 'Invest'
        : 'Withdraw'}
    </BalButton>
  );

  const renderExplorerLinkButton = () => (
    <Link
      href={`${EXPLORERS[chain?.id || BALANCER_DEFAULT_CHAIN_ID]}/tx/${
        receipt?.transactionHash
      }`}
      _hover={{ color: 'balancer.purple' }}
      isExternal
    >
      <BalText decoration="underline">
        View transaction on {EXPLORERS[chain?.id || BALANCER_DEFAULT_CHAIN_ID]}
      </BalText>
    </Link>
  );

  const renderConfetti = () => (
    <ReactCanvasConfetti refConfetti={getInstance} style={canvasStyles} />
  );

  return (
    <BalModal
      title={
        state === BalState.Submitted
          ? `✅ ${
              investType === 'deposit' ? 'Investment' : 'Withdrawal'
            } confirmed`
          : `${investType === 'deposit' ? 'Investment' : 'Withdrawal'} Preview`
      }
      isOpen={isOpen}
      onClose={onClose}
      p={6}
      mt={-6}
      {...props}
    >
      <VStack spacing={6}>
        <StackItem w="full">{renderTokenAmounts()}</StackItem>
        <StackItem w="full">{renderSummary()}</StackItem>
        {isRenderSteps && <StackItem w="full">{renderSteps()}</StackItem>}
        {state === BalState.Submitted && (
          <StackItem w="full" h="20px">
            {renderExplorerLinkButton()}
          </StackItem>
        )}
        <StackItem w="full">{renderConfirmButton()}</StackItem>
      </VStack>
      {renderConfetti()}
    </BalModal>
  );
};
