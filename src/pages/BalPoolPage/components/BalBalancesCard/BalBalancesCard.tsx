import { HStack, StackItem, VStack } from '@chakra-ui/react';
import {
  TokenWithBalance,
  TokensPriceUsdQueryResponse,
  useLogger,
} from '@aw-balancer/hooks';
import { BalCard, IBalCard, BalText } from '@aw-balancer/components';
import { BalToken } from '@aw-balancer/types';
import { useEffect, useState } from 'react';
import { BalBalancesCardItem } from './BalBalancesCardItem';

export interface IBalBalancesCard extends IBalCard {
  tokens: BalToken[];
  balances?: TokenWithBalance[] | undefined;
  tokenPriceUsdByAddress?: TokensPriceUsdQueryResponse | undefined;
}

export const BalBalancesCard = ({
  headerText,
  tokens,
  balances,
  tokenPriceUsdByAddress,
  ...props
}: IBalBalancesCard) => {
  /**
   * logging
   */
  const log = useLogger(BalBalancesCard.name);

  /**
   * balances
   */
  const [totalBalanceUsd, setTotalBalanceUsd] = useState(0);

  useEffect(() => {
    log.verbose('CALC_TOTAL_BALANCE_USD', balances, tokenPriceUsdByAddress);
    if (!tokenPriceUsdByAddress) return setTotalBalanceUsd(0);
    let _totalBalanceUsd = 0;
    balances?.map((tokenBalance) => {
      const tokenPriceUsd = tokenPriceUsdByAddress[tokenBalance.address];
      if (!tokenPriceUsd) return;
      const tokenBalanceUsd = Number(tokenBalance.formatted) * tokenPriceUsd;
      _totalBalanceUsd += tokenBalanceUsd;
    });
    setTotalBalanceUsd(_totalBalanceUsd);
  }, [balances, tokenPriceUsdByAddress]);

  /**
   * UI
   */
  const renderTotalBalanceUsd = () => (
    <HStack mt={2} justifyContent="space-between">
      <StackItem>
        <BalText>Total</BalText>
      </StackItem>
      <StackItem>
        <BalText>${totalBalanceUsd.toFixed(2)}</BalText>
      </StackItem>
    </HStack>
  );

  const renderBalancesCardItem = (token: BalToken) => (
    <BalBalancesCardItem
      key={token.address}
      token={token}
      balance={balances?.find((balance) => balance.address === token.address)}
      tokenPriceUsd={tokenPriceUsdByAddress?.[token.address]}
    />
  );

  return (
    <BalCard headerText={headerText} {...props}>
      <VStack spacing={4}>
        {tokens.map((token) => (
          <StackItem w="full" key={token.address}>
            {renderBalancesCardItem(token)}
          </StackItem>
        ))}
        <StackItem w="full">{renderTotalBalanceUsd()}</StackItem>
      </VStack>
    </BalCard>
  );
};
