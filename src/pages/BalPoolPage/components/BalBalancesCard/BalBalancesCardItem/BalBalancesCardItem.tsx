import { HStack, VStack, StackItem, StackProps } from '@chakra-ui/react';
import { TokenWithBalance } from '@aw-balancer/hooks';
import { BalText } from '@aw-balancer/components';
import { BalToken } from '@aw-balancer/types';
import { useEffect, useState } from 'react';

export interface IBalBalancesCardItem extends Omit<StackProps, 'children'> {
  token: BalToken;
  balance?: TokenWithBalance;
  tokenPriceUsd?: number;
}

export const BalBalancesCardItem = ({
  token,
  balance,
  tokenPriceUsd,
  ...props
}: IBalBalancesCardItem) => {
  /**
   * balance
   */
  const [balanceUsd, setBalanceUsd] = useState(0);
  useEffect(() => {
    const balanceUsd = Number(balance?.formatted || 0) * (tokenPriceUsd || 0);
    setBalanceUsd(balanceUsd);
  }, [balance, tokenPriceUsd]);

  return (
    <VStack lineHeight={4} {...props}>
      <StackItem w="full">
        <HStack justifyContent="space-between">
          <StackItem>
            <BalText>{token.symbol}</BalText>
          </StackItem>
          <StackItem>
            <BalText>{balance?.formatted?.substring(0, 6) || '0.0'}</BalText>
          </StackItem>
        </HStack>
      </StackItem>

      <StackItem w="full">
        <HStack justifyContent="space-between">
          <StackItem maxW="70%">
            <BalText fontSize="lg" opacity=".5">
              {token.name}
            </BalText>
          </StackItem>
          <StackItem>
            <BalText fontSize="lg" opacity=".5">
              ${balanceUsd.toFixed(2)}
            </BalText>
          </StackItem>
        </HStack>
      </StackItem>
    </VStack>
  );
};
