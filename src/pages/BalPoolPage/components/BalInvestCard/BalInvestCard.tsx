import { HStack, StackItem, VStack } from '@chakra-ui/react';
import {
  TokenWithBalance,
  TokensPriceUsdQueryResponse,
} from '@aw-balancer/hooks';
import {
  BalCard,
  IBalCard,
  BalText,
  BalTokenAmountInput,
  BalButton,
} from '@aw-balancer/components';
import { BalInvestType, BalTokenAmount } from '@aw-balancer/types';

export interface IBalInvestCard extends IBalCard {
  investType: BalInvestType;
  tokenAmounts: BalTokenAmount[];
  totalAmountUsd: number;
  walletBalances?: TokenWithBalance[] | undefined;
  poolBalances?: TokenWithBalance[] | undefined;
  tokenPriceUsdByAddress?: TokensPriceUsdQueryResponse | undefined;
  handlePreviewButtonClick: () => void;
  setTokenAmounts: (value: React.SetStateAction<BalTokenAmount[]>) => void;
}

export const BalInvestCard = ({
  investType,
  tokenAmounts,
  totalAmountUsd,
  walletBalances,
  poolBalances,
  tokenPriceUsdByAddress,
  handlePreviewButtonClick,
  setTokenAmounts,
  ...props
}: IBalInvestCard) => {
  /**
   * handles
   */
  const handleTokenAmountChange = (
    newAmount: string,
    tokenAmount: BalTokenAmount
  ) => {
    setTokenAmounts(
      tokenAmounts.map(
        (_tokenAmount) =>
          new BalTokenAmount(
            _tokenAmount.token,
            _tokenAmount.token.address === tokenAmount.token.address
              ? newAmount
              : _tokenAmount.amount
          )
      )
    );
  };

  const handleMaxTotalAmountButtonClick = () =>
    setTokenAmounts(
      tokenAmounts.map(
        (tokenAmount) =>
          new BalTokenAmount(
            tokenAmount.token,
            (investType === 'deposit' ? walletBalances : poolBalances)?.find(
              (tokenBalance) =>
                tokenBalance.address === tokenAmount.token.address
            )?.formatted || ''
          )
      )
    );

  /**
   * UI
   */
  const renderHeader = () => (
    <HStack alignItems="center" justifyContent="space-between">
      <StackItem>
        <BalText as="h1" fontSize="3xl">
          {investType === 'deposit' ? 'Invest in pool' : 'Withdraw from pool'}
        </BalText>
      </StackItem>
      {/* <StackItem>
            <BalButton>Settings</BalButton>
          </StackItem> */}
    </HStack>
  );

  const renderTokenAmountInputs = () => (
    <VStack spacing={3}>
      {tokenAmounts.map((tokenAmount) => (
        <StackItem key={tokenAmount.token.address} w="full">
          <BalTokenAmountInput
            tokenAmount={tokenAmount}
            balance={
              (investType === 'deposit' ? walletBalances : poolBalances)?.find(
                (balance) => balance.address === tokenAmount.token.address
              )?.formatted
            }
            tokenPriceUsd={tokenPriceUsdByAddress?.[tokenAmount.token.address]}
            onAmountChange={(newAmount) =>
              handleTokenAmountChange(newAmount, tokenAmount)
            }
          />
        </StackItem>
      ))}
    </VStack>
  );

  const renderSummary = () => (
    <VStack spacing={0}>
      <StackItem w="full">
        <HStack justifyContent="space-between">
          <StackItem>
            <BalText fontSize="3xl">Total</BalText>
          </StackItem>
          <StackItem>
            <HStack>
              <StackItem>
                <BalText>${totalAmountUsd.toFixed(2)}</BalText>
              </StackItem>
              <StackItem>
                <BalButton
                  variant="unstyled"
                  color="balancer.blue"
                  onClick={handleMaxTotalAmountButtonClick}
                >
                  Max
                </BalButton>
              </StackItem>
            </HStack>
          </StackItem>
        </HStack>
      </StackItem>
      <StackItem w="full">
        <HStack justifyContent="space-between">
          <StackItem>
            <BalText>Price Impact</BalText>
          </StackItem>
          <StackItem>
            <BalText>-.--%</BalText>
          </StackItem>
        </HStack>
      </StackItem>
    </VStack>
  );

  const renderPreviewButton = () => (
    <BalButton
      w="full"
      size="lg"
      bg="balancer.gradient"
      _hover={{ bg: '' }}
      _active={{ bg: '' }}
      disabled={totalAmountUsd === 0}
      onClick={handlePreviewButtonClick}
    >
      Preview
    </BalButton>
  );

  return (
    <BalCard header={renderHeader} {...props}>
      <VStack spacing={3}>
        <StackItem w="full">{renderTokenAmountInputs()}</StackItem>
        <StackItem w="full">{renderSummary()}</StackItem>
        <StackItem w="full">{renderPreviewButton()}</StackItem>
      </VStack>
    </BalCard>
  );
};
