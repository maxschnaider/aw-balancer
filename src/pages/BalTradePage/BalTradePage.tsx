import {
  useApprove,
  useCheckAllowance,
  useLogger,
  useTokensPriceUsd,
} from '@aw-balancer/hooks';
import { useEffect, useMemo, useState } from 'react';
import { StackItem, StackProps, useDisclosure, VStack } from '@chakra-ui/react';
import {
  BalTradeSide,
  BalTokenAmount,
  BalState,
  BalToken,
} from '@aw-balancer/types';
import { constants } from 'ethers';
import {
  BALANCER_DEFAULT_CHAIN_ID,
  BALANCER_SUPPORTED_CHAIN_IDS,
  BALANCER_VAULT_ADDRESS,
} from '@aw-balancer/constants';
import {
  useAccount,
  useBalance,
  useNetwork,
  useSigner,
  useWaitForTransaction,
} from 'wagmi';
import { useToaster } from '@aw-balancer/hooks';
import { BalTokensModal } from '@aw-balancer/components';
import {
  BalMyWalletCard,
  BalTradeCard,
  BalTradeConfirmModal,
  BalTradeSubmittedModal,
} from '@aw-balancer/pages';
import { useBalQuoteSwap, useBalSwap } from '@aw-balancer/hooks';

export interface IBalTradetView extends Omit<StackProps, 'children'> {}

export const BalTradePage = ({ ...props }: IBalTradetView) => {
  /**
   * utils
   */
  const log = useLogger(BalTradePage.name);
  const toaster = useToaster();

  /**
   * web3
   */
  const { data: signer } = useSigner();
  const { chain: activeChain } = useNetwork();
  const { address } = useAccount();

  const isSupportedChain = useMemo(
    () =>
      !!activeChain &&
      Object.values(BALANCER_SUPPORTED_CHAIN_IDS).includes(activeChain.id),
    [activeChain]
  );

  /**
   * tokens
   */
  const [tokenIn, setTokenIn] = useState<BalToken | undefined>();
  const [tokenOut, setTokenOut] = useState<BalToken | undefined>();

  const switchTokens = () => {
    resetAmount();
    const oldFromToken = tokenIn;
    setTokenIn(tokenOut);
    setTokenOut(oldFromToken);
  };

  /**
   * balance
   */
  const { data: tokenInBalance } = useBalance({
    addressOrName: address,
    token: tokenIn?.address,
    formatUnits: tokenIn?.decimals || 18,
    watch: true,
    chainId: isSupportedChain ? activeChain?.id : BALANCER_DEFAULT_CHAIN_ID,
    enabled: !!tokenIn,
  });

  const { data: tokenOutBalance } = useBalance({
    addressOrName: address,
    token: tokenOut?.address,
    formatUnits: tokenOut?.decimals || 18,
    watch: true,
    chainId: isSupportedChain ? activeChain?.id : BALANCER_DEFAULT_CHAIN_ID,
    enabled: !!tokenOut,
  });

  const { data: tokenPriceUsdByAddress } = useTokensPriceUsd({
    addresses: [
      tokenIn?.address || constants.AddressZero,
      tokenOut?.address || constants.AddressZero,
    ],
  });

  /**
   * amount
   */
  const [amount, setAmount] = useState<string | undefined>();

  const tokenAmountIn = useMemo(
    () => (!!tokenIn ? new BalTokenAmount(tokenIn, amount) : undefined),
    [tokenIn, amount]
  );
  const tokenAmountOut = useMemo(
    () => (!!tokenOut ? new BalTokenAmount(tokenOut) : undefined),
    [tokenOut]
  );

  const resetAmount = () => {
    setAmount(undefined);
  };

  const isValidAmount = useMemo(
    () =>
      Number(tokenAmountIn?.amount) > 0 &&
      Number(tokenAmountIn?.amount) <= Number(tokenInBalance?.formatted),
    [tokenAmountIn, tokenInBalance]
  );

  /**
   * allowance
   */
  const { data: allowance, refetch: checkAllowance } = useCheckAllowance({
    address: tokenAmountIn?.token.address,
    account: address,
    spender: BALANCER_VAULT_ADDRESS,
    enabled: !!tokenAmountIn,
  });

  const isAmountExceedAllowance = useMemo(
    () =>
      !!allowance &&
      !!tokenAmountIn &&
      tokenAmountIn.toBigNumber().gt(allowance),
    [allowance, tokenAmountIn]
  );

  const {
    data: approveTx,
    isLoading: isSigningApprove,
    mutate: approve,
    reset: resetApproveTx,
  } = useApprove({
    token: tokenAmountIn?.token.address || constants.AddressZero,
  });

  const { isLoading: isLoadingApprove } = useWaitForTransaction({
    hash: approveTx?.hash,
    confirmations: 7,
    onSuccess: (approveReceipt) => {
      if (approveReceipt.status === 1) {
        log.success('APPROVE_TX_SUCC', approveReceipt);
        toaster.showApproveSuccessToast();
      } else {
        log.error('APPROVE_TX_ERROR', approveReceipt);
        toaster.showApproveErrorToast();
      }
      resetApproveTx();
      checkAllowance();
    },
    onError: (error) => {
      log.error('APPROVE_TX_ERROR', error);
      toaster.showApproveErrorToast();
    },
  });

  const handleApprove = () => {
    if (!tokenAmountIn) return;
    log.verbose('HANDLE_APPROVE', tokenAmountIn);
    approve({
      spender: BALANCER_VAULT_ADDRESS,
      value: tokenAmountIn.toBigNumber(),
    });
  };

  useEffect(() => {
    checkAllowance();
  }, [approveTx, tokenAmountIn]);

  /**
   * swap
   */
  const { data: swapInfo } = useBalQuoteSwap({
    tokenAmountIn: tokenAmountIn,
    tokenOut: tokenOut,
  });

  const {
    data: swapTx,
    isLoading: isSigningSwap,
    reset: resetSwapTx,
    mutate: swap,
  } = useBalSwap();

  const { isLoading: isLoadingSwap } = useWaitForTransaction({
    hash: '',
    onSuccess: (swapReceipt) => {
      if (swapReceipt.status === 1) {
        log.success('SWAP_TX_SUCC', swapReceipt);
        toaster.showSwapSuccessToast();
      } else {
        log.error('SWAP_TX_ERROR', swapReceipt);
        toaster.showSwapErrorToast();
      }
    },
    onError: (error) => {
      log.error('SWAP_TX_ERROR', error);
      toaster.showSwapErrorToast();
    },
  });

  /**
   * state
   */
  const currentState = useMemo(() => {
    if (!signer) {
      return BalState.NoSigner;
    }
    if (!isSupportedChain) {
      return BalState.WrongNetwork;
    }
    if (!tokenIn || !tokenOut) {
      return BalState.NoTokens;
    }
    if (!isValidAmount) {
      return BalState.InvalidAmount;
    }
    if (
      isSigningApprove ||
      isLoadingApprove ||
      isSigningSwap ||
      isLoadingSwap
    ) {
      return BalState.Loading;
    }
    if (isAmountExceedAllowance) {
      return BalState.ApproveRequired;
    }
    if (!!swapTx) {
      return BalState.Submitted;
    }
    return BalState.Ready;
  }, [
    signer,
    isSupportedChain,
    tokenIn,
    tokenOut,
    isValidAmount,
    isAmountExceedAllowance,
    isSigningApprove,
    isLoadingApprove,
    isSigningSwap,
    isLoadingSwap,
    swapTx,
  ]);

  const resetState = () => {
    resetSwapTx();
    resetAmount();
  };

  /**
   * UI
   */
  const renderTradeCard = () => (
    <BalTradeCard
      state={currentState}
      tokenAmountIn={tokenAmountIn}
      tokenAmountOut={swapInfo?.tokenAmountOut || tokenAmountOut}
      tokenInBalance={tokenInBalance}
      tokenOutBalance={tokenOutBalance}
      tokenPriceUsdByAddress={tokenPriceUsdByAddress}
      swapRate={swapInfo?.rate}
      onAmountChange={(newAmount) => {
        setAmount(newAmount);
      }}
      onTokenButtonClick={(side) => {
        setSelectionSide(side);
        openTokensModal();
      }}
      onSwitchButtonClick={switchTokens}
      onPreviewButtonClick={openConfirmModal}
    />
  );

  const renderMyWalletCard = () => (
    <BalMyWalletCard
      onTokenSelect={(token) => {
        setTokenIn(token);
      }}
    />
  );

  const {
    isOpen: isTokensModalOpen,
    onOpen: openTokensModal,
    onClose: closeTokensModal,
  } = useDisclosure();

  const [selectionSide, setSelectionSide] = useState<BalTradeSide>('in');

  const renderTokensModal = () => (
    <BalTokensModal
      isOpen={isTokensModalOpen}
      onClose={closeTokensModal}
      onTokenSelect={(token) => {
        closeTokensModal();
        selectionSide === 'in' ? setTokenIn(token) : setTokenOut(token);
      }}
    />
  );

  const {
    isOpen: isConfirmModalOpen,
    onOpen: openConfirmModal,
    onClose: closeConfirmModal,
  } = useDisclosure();

  const renderConfirmModal = () => (
    <BalTradeConfirmModal
      state={currentState}
      swapInfo={swapInfo}
      tokenPriceUsdByAddress={tokenPriceUsdByAddress}
      onConfirmButtonClick={() => {
        if (currentState === BalState.ApproveRequired) {
          handleApprove();
        } else if (currentState === BalState.Ready) {
          swap();
        }
      }}
      isOpen={isConfirmModalOpen}
      onClose={closeConfirmModal}
    />
  );

  const {
    isOpen: isSubmittedModalOpen,
    onOpen: openSubmittedModal,
    onClose: closeSubmittedModal,
  } = useDisclosure();

  const renderSubmittedModal = () => (
    <BalTradeSubmittedModal
      tx={undefined}
      isOpen={isSubmittedModalOpen}
      onClose={() => {
        closeSubmittedModal();
        resetState();
      }}
    />
  );

  return (
    <>
      <VStack spacing={4} p={4} alignItems="start" {...props}>
        <StackItem w="full">{renderTradeCard()}</StackItem>
        <StackItem w="full">{renderMyWalletCard()}</StackItem>
      </VStack>
      {renderTokensModal()}
      {renderConfirmModal()}
      {renderSubmittedModal()}
    </>
  );
};
