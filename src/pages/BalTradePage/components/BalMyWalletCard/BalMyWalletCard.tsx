import { useTokenBalances } from '@aw-balancer/hooks';
import { TokenAvatar } from '@aw-balancer/components';
import { BalCard, BalText, IBalCard } from '@aw-balancer/components';
import { useBalTokenList } from '@aw-balancer/hooks';
import { BalToken } from '@aw-balancer/types';
import { Divider, Flex, Skeleton } from '@chakra-ui/react';
import { useMemo } from 'react';
import { useAccount, useBalance, useNetwork } from 'wagmi';

interface IBalMyWalletCard extends IBalCard {
  onTokenSelect: (token: BalToken) => void;
}

export const BalMyWalletCard = ({
  onTokenSelect,
  ...props
}: IBalMyWalletCard) => {
  /**
   * web3
   */
  const { address } = useAccount();
  const { chain } = useNetwork();

  /**
   * balance
   */
  const { data: ethBalance, isLoading: isLoadingBalance } = useBalance({
    addressOrName: address,
    watch: true,
  });

  /**
   * tokens
   */
  const { data: tokens } = useBalTokenList();
  const { data: balances } = useTokenBalances({
    addresses: tokens?.map((token) => token.address) || [],
  });
  const tokensInWallet = useMemo(() => {
    const _tokensInWallet: BalToken[] = [];
    balances?.forEach((balance) => {
      if (Number(balance.formatted) > 0) {
        const token = tokens?.find(
          (token) =>
            token.address.toLowerCase() === balance.address.toLowerCase()
        );
        if (!!token) {
          _tokensInWallet.push(token);
        }
      }
    });
    return _tokensInWallet;
  }, [balances, tokens]);

  /**
   * UI
   */
  const renderBalance = () =>
    isLoadingBalance ? (
      <Skeleton h="15px" w="60px" />
    ) : (
      <BalText fontSize="2xl">
        {ethBalance?.formatted?.substring(0, 6) || '0.0'}{' '}
        {chain?.nativeCurrency?.symbol}
      </BalText>
    );

  const renderHeader = () => (
    <Flex justifyContent="space-between">
      <BalText fontSize="2xl">My wallet</BalText>
      {renderBalance()}
    </Flex>
  );

  const renderTokens = () => (
    <Flex gap={2} p={1} overflowX="auto" overflowY="hidden">
      {tokensInWallet.map((token) => (
        <TokenAvatar
          key={token.address}
          logoURI={token.logoURI}
          address={token.address}
          name={token.symbol}
          size="sm"
          cursor="pointer"
          transition=".2s"
          _hover={{ transform: 'scale(1.1)' }}
          onClick={() => onTokenSelect(token)}
        />
      ))}
    </Flex>
  );

  return (
    <BalCard header={renderHeader} {...props}>
      {tokensInWallet.length > 0 && (
        <>
          <Divider mb={3} mt={1} borderColor="blackAlpha.700" />
          {renderTokens()}
        </>
      )}
    </BalCard>
  );
};
