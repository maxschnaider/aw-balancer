import { Link, StackItem, VStack } from '@chakra-ui/react';
import {
  BalButton,
  BalText,
  BalModal,
  IBalModal,
} from '@aw-balancer/components';
import { useEffect } from 'react';
import { BALANCER_DEFAULT_CHAIN_ID, EXPLORERS } from '@aw-balancer/constants';
import { ContractTransaction } from 'ethers';
import ReactCanvasConfetti from 'react-canvas-confetti';
import { useConfetti } from '@aw-balancer/hooks';
import { useNetwork } from 'wagmi';

export interface IBalTradeSubmittedModal
  extends Omit<IBalModal, 'title' | 'children'> {
  tx?: ContractTransaction | undefined;
}

export const BalTradeSubmittedModal = ({
  tx,
  isOpen,
  onClose,
  ...props
}: IBalTradeSubmittedModal) => {
  /**
   * web3
   */
  const { chain } = useNetwork();

  /**
   * confetti
   */
  const { getInstance, canvasStyles, startAnimation, stopAnimation } =
    useConfetti();

  useEffect(() => {
    if (isOpen) {
      startAnimation();
      setTimeout(stopAnimation, 6000);
    }
  }, [isOpen]);

  /**
   * UI
   */
  const renderExplorerLinkButton = () => (
    <Link
      href={`${EXPLORERS[chain?.id || BALANCER_DEFAULT_CHAIN_ID]}/tx/${
        tx?.hash
      }`}
      _hover={{ color: 'balancer.purple' }}
      isExternal
    >
      <BalText decoration="underline">
        View transaction on {EXPLORERS[chain?.id || BALANCER_DEFAULT_CHAIN_ID]}
      </BalText>
    </Link>
  );

  const renderDoneButton = () => (
    <BalButton
      w="full"
      bg="balancer.gradient"
      _hover={{ bg: '' }}
      _active={{ bg: '' }}
      onClick={onClose}
    >
      Done
    </BalButton>
  );

  const renderConfetti = () => (
    <ReactCanvasConfetti refConfetti={getInstance} style={canvasStyles} />
  );

  return (
    <BalModal
      title="✅ Transaction submitted"
      isOpen={isOpen}
      onClose={onClose}
      p={6}
      mt={-6}
      {...props}
    >
      <VStack spacing={4}>
        <StackItem w="full">{renderExplorerLinkButton()}</StackItem>
        <StackItem w="full">{renderDoneButton()}</StackItem>
      </VStack>
      {renderConfetti()}
    </BalModal>
  );
};
