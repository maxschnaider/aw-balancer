import { TokensPriceUsdQueryResponse } from '@aw-balancer/hooks';
import {
  BalButton,
  BalCard,
  BalText,
  BalTokenAmountInput,
  IBalCard,
} from '@aw-balancer/components';
import { BalState, BalTokenAmount, BalTradeSide } from '@aw-balancer/types';
import { Flex, IconButton, StackItem, VStack } from '@chakra-ui/react';
import { FetchBalanceResult } from '@wagmi/core';
import { constants } from 'ethers';
import { MdSwapVert } from 'react-icons/md';

interface IBalTradeCard extends IBalCard {
  state: BalState;
  tokenAmountIn?: BalTokenAmount | undefined;
  tokenAmountOut?: BalTokenAmount | undefined;
  tokenInBalance?: FetchBalanceResult | undefined;
  tokenOutBalance?: FetchBalanceResult | undefined;
  tokenPriceUsdByAddress?: TokensPriceUsdQueryResponse | undefined;
  swapRate?: number | undefined;
  onAmountChange: (newAmount: string) => void;
  onTokenButtonClick: (side: BalTradeSide) => void;
  onSwitchButtonClick: () => void;
  onPreviewButtonClick: () => void;
}

export const BalTradeCard = ({
  state,
  tokenAmountIn,
  tokenAmountOut,
  tokenInBalance,
  tokenOutBalance,
  tokenPriceUsdByAddress,
  swapRate,
  onAmountChange,
  onTokenButtonClick,
  onSwitchButtonClick,
  onPreviewButtonClick,
  ...props
}: IBalTradeCard) => {
  /**
   * UI
   */
  const renderHeader = () => <BalText fontSize="2xl">Trade</BalText>;

  const renderTokenAmountInput = (side: BalTradeSide) => {
    const tokenAmount = side === 'in' ? tokenAmountIn : tokenAmountOut;
    const balance = side === 'in' ? tokenInBalance : tokenOutBalance;
    return (
      <BalTokenAmountInput
        tokenAmount={tokenAmount}
        balance={balance?.formatted}
        tokenPriceUsd={
          tokenPriceUsdByAddress?.[
            tokenAmount?.token.address || constants.AddressZero
          ]
        }
        readonly={side === 'out'}
        onAmountChange={onAmountChange}
        onTokenButtonClick={() => onTokenButtonClick(side)}
      />
    );
  };

  const renderSwitchButton = () => (
    <IconButton
      aria-label="Switch tokens"
      borderRadius="full"
      icon={<MdSwapVert />}
      bg="balancer.bg"
      size="sm"
      _hover={{ fontSize: 'lg', color: 'balancer.yellow' }}
      transition=".2s"
      onClick={onSwitchButtonClick}
    />
  );

  const renderSwapRate = () => (
    <BalText fontSize="md" opacity="0.5">
      1 {tokenAmountIn?.token.symbol} = {swapRate}{' '}
      {tokenAmountOut?.token.symbol}
    </BalText>
  );

  const renderPreviewButton = () => (
    <BalButton
      w="full"
      size="lg"
      bg="balancer.gradient"
      _hover={{ bg: '' }}
      _active={{ bg: '' }}
      disabled={state !== BalState.Ready && state !== BalState.ApproveRequired}
      onClick={onPreviewButtonClick}
    >
      Preview
    </BalButton>
  );

  return (
    <BalCard header={renderHeader} {...props}>
      <VStack>
        <StackItem w="full">{renderTokenAmountInput('in')}</StackItem>
        <StackItem w="full">
          <Flex justifyContent="space-between" alignItems="center">
            {renderSwitchButton()}
            {!!swapRate && renderSwapRate()}
          </Flex>
        </StackItem>
        <StackItem w="full">{renderTokenAmountInput('out')}</StackItem>
        <StackItem w="full">{renderPreviewButton()}</StackItem>
      </VStack>
    </BalCard>
  );
};
