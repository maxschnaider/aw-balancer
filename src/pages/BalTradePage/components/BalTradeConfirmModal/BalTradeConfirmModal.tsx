import {
  Divider,
  Flex,
  HStack,
  StackDivider,
  StackItem,
  VStack,
} from '@chakra-ui/react';
import { BalState, BalTokenAmount, BalSwapInfo } from '@aw-balancer/types';
import {
  BalButton,
  BalText,
  BalModal,
  IBalModal,
} from '@aw-balancer/components';
import { TokenAvatar } from '@aw-balancer/components';
import { TokensPriceUsdQueryResponse } from '@aw-balancer/hooks';
import { useEffect, useState } from 'react';

export interface IBalTradeConfirmModal
  extends Omit<IBalModal, 'title' | 'children'> {
  state: BalState;
  swapInfo?: BalSwapInfo;
  tokenPriceUsdByAddress?: TokensPriceUsdQueryResponse | undefined;
  onConfirmButtonClick: () => void;
}

export const BalTradeConfirmModal = ({
  state,
  swapInfo,
  tokenPriceUsdByAddress,
  onConfirmButtonClick,
  isOpen,
  onClose,
  ...props
}: IBalTradeConfirmModal) => {
  /**
   * steps
   */
  const [isDisplaySteps, setIsDisplaySteps] = useState(false);

  useEffect(() => {
    setIsDisplaySteps(state === BalState.ApproveRequired);
  }, [isOpen]);

  /**
   * UI
   */
  const renderTokenAmount = (tokenAmount?: BalTokenAmount) => {
    const tokenPriceUsd = !!tokenAmount
      ? tokenPriceUsdByAddress?.[tokenAmount.token.address]
      : 0;
    const tokenAmountUsd =
      Number(tokenAmount?.amount || 0) * (tokenPriceUsd || 0);
    return (
      <HStack px={3} py={2} spacing={4}>
        <StackItem>
          <TokenAvatar
            logoURI={tokenAmount?.token.logoURI}
            address={tokenAmount?.token.address}
            name={tokenAmount?.token.symbol}
            size="sm"
          />
        </StackItem>
        <StackItem>
          <VStack alignItems="start" spacing={-2}>
            <StackItem>
              <BalText>
                {Number(tokenAmount?.amount) > 0
                  ? tokenAmount?.amount?.substring(0, 6)
                  : '0.0'}{' '}
                {tokenAmount?.token.symbol}
              </BalText>
            </StackItem>
            <StackItem>
              <BalText fontSize="lg" opacity="0.5">
                <>${tokenAmountUsd?.toFixed(2) || '0.00'}</>
              </BalText>
            </StackItem>
          </VStack>
        </StackItem>
      </HStack>
    );
  };

  const renderTokenAmounts = () => (
    <VStack
      divider={<StackDivider />}
      spacing={0}
      borderWidth="1px"
      borderRadius="lg"
      borderColor="blackAlpha.500"
    >
      <StackItem
        w="full"
        bg="balancer.bgLighter"
        borderTopRadius="lg"
        px={3}
        py={1}
      >
        <BalText fontSize="lg">
          Effective price: 1 {swapInfo?.tokenAmountIn.token.symbol} = 1.022{' '}
          {swapInfo?.tokenAmountOut.token.symbol}
        </BalText>
      </StackItem>
      <StackItem w="full">
        {renderTokenAmount(swapInfo?.tokenAmountIn)}
      </StackItem>
      <StackItem w="full">
        {renderTokenAmount(swapInfo?.tokenAmountOut)}
      </StackItem>
    </VStack>
  );

  const renderDetails = () => (
    <VStack
      divider={<StackDivider />}
      spacing={0}
      borderWidth="1px"
      borderRadius="lg"
      borderColor="blackAlpha.500"
    >
      <StackItem w="full" px={3} py={1}>
        <BalText>
          Trade from {swapInfo?.tokenAmountIn.token.symbol} details
        </BalText>
      </StackItem>
      <StackItem w="full" px={3} py={1}>
        <VStack spacing={-1}>
          <StackItem w="full" display="flex" justifyContent="space-between">
            <BalText fontSize="lg">Total to receive before fees</BalText>
            <BalText fontSize="lg">
              0.0235 {swapInfo?.tokenAmountOut.token.symbol}
            </BalText>
          </StackItem>
          <StackItem
            w="full"
            display="flex"
            justifyContent="space-between"
            opacity="0.5"
          >
            <BalText fontSize="lg">Trade fees</BalText>
            <BalText fontSize="lg">
              0.0235 {swapInfo?.tokenAmountOut.token.symbol}
            </BalText>
          </StackItem>
        </VStack>
      </StackItem>
      <StackItem
        w="full"
        px={3}
        py={1}
        bg="balancer.bgLighter"
        borderBottomRadius="lg"
      >
        <VStack spacing={-1}>
          <StackItem w="full" display="flex" justifyContent="space-between">
            <BalText fontSize="lg">Total expected after fees</BalText>
            <BalText fontSize="lg">
              0.0235 {swapInfo?.tokenAmountOut.token.symbol}
            </BalText>
          </StackItem>
          <StackItem
            w="full"
            display="flex"
            justifyContent="space-between"
            opacity="0.5"
          >
            <BalText fontSize="lg">
              The least you’ll get at 1.00% slippage
            </BalText>
            <BalText fontSize="lg">
              0.0232 {swapInfo?.tokenAmountOut.token.symbol}
            </BalText>
          </StackItem>
        </VStack>
      </StackItem>
    </VStack>
  );

  const renderSteps = () => (
    <HStack spacing={0} justifyContent="center" alignItems="center">
      {new Array(2).fill(0).map((_, index) => (
        <>
          <StackItem key={index * 2}>
            <Flex
              w="25px"
              h="25px"
              borderRadius="full"
              justifyContent="center"
              alignItems="center"
              border="1px solid"
              borderColor={
                index === 0
                  ? state === BalState.Ready
                    ? 'green.300'
                    : 'balancer.purple'
                  : 'whiteAlpha.300'
              }
              color={
                index === 0 && state === BalState.Ready
                  ? 'green.300'
                  : 'balancer.purple'
              }
              fontSize="xl"
            >
              {index === 0 && state === BalState.Ready ? '✓' : index + 1}
            </Flex>
          </StackItem>
          {index === 0 && (
            <StackItem key={index * 2 + 1}>
              <Divider w="20px" />
            </StackItem>
          )}
        </>
      ))}
    </HStack>
  );

  const renderConfirmButton = () => (
    <BalButton
      bg="balancer.gradient"
      _hover={{ bg: '' }}
      _active={{ bg: '' }}
      color="white"
      size="lg"
      w="full"
      onClick={onConfirmButtonClick}
      isLoading={state === BalState.Loading}
    >
      {state === BalState.ApproveRequired
        ? `Approve ${swapInfo?.tokenAmountIn.token.symbol}`
        : 'Trade'}
    </BalButton>
  );

  return (
    <BalModal
      title="Preview trade"
      isOpen={isOpen}
      onClose={onClose}
      p={6}
      mt={-6}
      {...props}
    >
      <VStack spacing={6}>
        <StackItem w="full">{renderTokenAmounts()}</StackItem>
        <StackItem w="full">{renderDetails()}</StackItem>
        {isDisplaySteps && <StackItem w="full">{renderSteps()}</StackItem>}
        <StackItem w="full">{renderConfirmButton()}</StackItem>
      </VStack>
    </BalModal>
  );
};
