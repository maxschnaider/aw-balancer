// ! Deprecated, to be deleted.
export interface RouteParams {
  [key: string]: any;
}

export interface Route {
  path: string;
  title?: string;
  params?: RouteParams;
}

export interface RouterState {
  current: Route;
  history: Route[];
  routes: {
    [key: string]: JSX.Element | undefined;
  };
}
