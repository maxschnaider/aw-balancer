import { Box, BoxProps } from '@chakra-ui/react';

interface IRouterContainer extends BoxProps {}

export const RouterContainer = ({ children, ...props }: IRouterContainer) => (
  <Box {...props} h="100%" overflowY="auto" w="full">
    {children}
  </Box>
);
