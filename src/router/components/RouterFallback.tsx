import React from 'react';

interface IRouterFallback {
  children?: React.ReactNode;
}

export const RouterFallback = ({ children }: IRouterFallback) => (
  <>{children || <>Not Found</>}</>
);
