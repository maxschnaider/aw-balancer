import { useEffect, useMemo } from 'react';
import { BoxProps } from '@chakra-ui/react';
import { StateWithValue } from 'react-ridge-state';
import { RouterState } from '@aw-balancer/router/types';
import { useLogger } from '@aw-balancer/hooks';
import { useRouter } from '@aw-balancer/router/hooks';
import {
  RouterContainer,
  RouterFallback,
} from '@aw-balancer/router/components';

interface IRouter extends BoxProps {
  state: StateWithValue<RouterState>;
  logVerbose?: boolean;
  fallback?: JSX.Element;
}

export const Router = ({ state, logVerbose, fallback, ...props }: IRouter) => {
  const log = useLogger(Router.name);
  const [{ active }] = useRouter(state);
  const routes = useMemo(() => state.get().routes, [active]);

  useEffect(() => {
    logVerbose && log?.verbose('active', active);
  }, [active, logVerbose]);

  return (
    <RouterContainer {...props}>
      {routes[active.path] || <RouterFallback children={fallback} />}
    </RouterContainer>
  );
};
