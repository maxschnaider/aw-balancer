import { useRouter } from '@aw-balancer/router';
import { BalancerFiRouterState } from '@aw-balancer/router/routes';

export const useBalRouter = () => useRouter(BalancerFiRouterState);
