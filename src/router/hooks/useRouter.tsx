import { useMemo } from 'react';
import { StateWithValue } from 'react-ridge-state';
import { Route, RouteParams, RouterState } from '@aw-balancer/router';

export const useRouter = (routerState: StateWithValue<RouterState>) => {
  const [state, setState] = routerState.use();
  const active = useMemo<Route>(() => state.current, [state]);
  const params = useMemo<RouteParams | undefined>(
    () => state.current.params,
    [state.current.params]
  );
  const history = useMemo<Route[]>(() => state.history, [state.history]);

  const navigate = (next: Route) => {
    const prev = state.current;
    setState({
      ...state,
      current: next,
      history: [...state.history, prev],
    });
  };

  const back = () => {
    const prev = [...state.history].pop();
    setState({
      ...state,
      current: {
        ...state.current,
        ...prev,
      },
      history: state.history.filter((route) => route.path !== prev?.path),
    });
  };

  return [
    { active, params, history },
    { navigate, back },
  ] as const;
};
