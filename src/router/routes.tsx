import { newRidgeState } from "react-ridge-state";
import { RouterState } from "@aw-balancer/router";
import { ReactElement } from "react";
import { BalInvestPage, BalPoolPage, BalTradePage } from "@aw-balancer/pages";

export enum BalancerFiView {
  Invest = "Invest",
  Pool = "Pool",
  Trade = "Trade",
}

const routes: Record<BalancerFiView, ReactElement> = {
  [BalancerFiView.Invest]: <BalInvestPage />,
  [BalancerFiView.Pool]: <BalPoolPage />,
  [BalancerFiView.Trade]: <BalTradePage />,
};

export const BalancerFiRouterState = newRidgeState<RouterState>({
  current: { path: BalancerFiView.Trade },
  history: [],
  routes,
});
