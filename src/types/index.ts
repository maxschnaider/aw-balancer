import { utils } from "ethers";

export interface BalToken {
  symbol: string;
  address: string;
  decimals: number;
  name?: string;
  logoURI?: string;
  weight?: string;
  chainId?: number;
}

export interface BalPool {
  id: string;
  address: string;
  poolType?: string;
  factory?: string;
  strategyType?: number;
  symbol?: string;
  name?: string;
  swapEnabled?: boolean;
  swapFee?: string;
  owner?: string;
  totalWeight?: string;
  totalSwapFee?: string;
  totalSwapVolume?: string;
  totalLiquidity?: string;
  totalShares?: string;
  createTime?: number;
  swapsCount?: string;
  holdersCount?: string;
  tokens?: BalToken[];
}

export type BalInvestType = "deposit" | "withdraw";

export type BalTradeSide = "in" | "out";

export class BalTokenAmount {
  token: BalToken;
  amount?: string;

  constructor(token: BalToken, amount?: string) {
    this.token = token;
    this.amount = amount;
  }

  toBigNumber = () => {
    const intDecimals: number | undefined = this.amount?.split(".")[0]?.length;
    const totalDecimals = (intDecimals || 1) + 1 + this.token.decimals;
    return utils.parseUnits(
      this.amount?.substring(0, totalDecimals) || "0",
      this.token.decimals
    );
  };
}

export interface BalSwapInfo {
  tokenAmountIn: BalTokenAmount;
  tokenAmountOut: BalTokenAmount;
  minimumAmountOut: string;
  rate: number;
  fee: number;
  route: BalPool[];
}

export enum BalState {
  NoSigner,
  WrongNetwork,
  NoTokens,
  InvalidAmount,
  ApproveRequired,
  Loading,
  Ready,
  Submitted,
  Idle,
}

// export type VIEW_STATUS = 'LIST' | 'VAULT'

// export type Token = {
//   name: string
//   symbol: string
//   address: string
//   decimals: number
//   display_name: string
//   icon: string
// }
// export type TVL = {
//   total_assets: BigNumberish
//   tvl: number
//   price: number
// }
// export type APY = {
//   type: string
//   gross_apr: number
//   net_apy: number
//   fees: {
//     performance: number | null
//     withdrawal: number | null
//     management: number | null
//     keep_crv: number | null
//     cvx_keep_crv: number | null
//   }
//   points?: {
//     week_ago: number
//     month_ago: number
//     inception: number
//   }
//   composite?: {
//     boost: number
//     pool_apy: number
//     boosted_apr: number
//     base_apr: number
//     cvx_apr: number
//     rewards_apr: number
//   }
// }
// export type VaultAPI = {
//   inception: number
//   address: string
//   symbol: string
//   name: string
//   display_name: string
//   icon: string
//   token: Token
//   tvl: TVL
//   apy: APY
//   endorsed: boolean
//   version: string
//   decimals: number
//   type: string
//   emergency_shutdown: boolean
//   updated: number
//   migration?: {
//     available: boolean
//     address: string
//   }
// }

// export type Vault = VaultAPI & {
//   chainID: number
//   description: string
//   categories: string[]
//   apy: {
//     net_apy: number
//     points: {
//       week_ago: number
//       month_ago: number
//       inception: number
//     }
//   }
// }
// export type Address = '/^0x([0-9a-f][0-9a-f])*$/I'
