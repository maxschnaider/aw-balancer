import { BALANCER_SUPPORTED_CHAIN_IDS } from "@aw-balancer/constants";

export const BALANCER_GRAPH_URL: Record<number, string> = {
  [BALANCER_SUPPORTED_CHAIN_IDS.Ethereum]:
    "https://api.thegraph.com/subgraphs/name/balancer-labs/balancer-v2",
  [BALANCER_SUPPORTED_CHAIN_IDS.Polygon]:
    "https://api.thegraph.com/subgraphs/name/balancer-labs/balancer-polygon-v2",
  [BALANCER_SUPPORTED_CHAIN_IDS.Arbitrum]:
    "https://api.thegraph.com/subgraphs/name/balancer-labs/balancer-arbitrum-v2",
};
