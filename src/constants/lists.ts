import { BALANCER_SUPPORTED_CHAIN_IDS } from "@aw-balancer/constants";

export const BALANCER_TOKEN_LIST_URI = {
  [BALANCER_SUPPORTED_CHAIN_IDS.Ethereum]:
    "https://raw.githubusercontent.com/balancer-labs/assets/master/generated/listed.tokenlist.json",
  [BALANCER_SUPPORTED_CHAIN_IDS.Polygon]:
    "https://raw.githubusercontent.com/balancer-labs/assets/refactor-for-multichain/generated/polygon.listed.tokenlist.json",
  [BALANCER_SUPPORTED_CHAIN_IDS.Arbitrum]:
    "https://raw.githubusercontent.com/balancer-labs/assets/refactor-for-multichain/generated/arbitrum.listed.tokenlist.json",
};
