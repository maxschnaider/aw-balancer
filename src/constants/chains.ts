import { chain } from "wagmi";

export type BalancerSupportedChain = "Ethereum" | "Polygon" | "Arbitrum";

export const BALANCER_SUPPORTED_CHAIN_IDS: Record<
  BalancerSupportedChain,
  number
> = {
  Ethereum: chain.mainnet.id,
  Polygon: chain.polygon.id,
  Arbitrum: chain.arbitrum.id,
};

export const BALANCER_DEFAULT_CHAIN_ID = BALANCER_SUPPORTED_CHAIN_IDS.Ethereum;

export const BALANCER_RPC_PROVIDERS = {
  [BALANCER_SUPPORTED_CHAIN_IDS.Ethereum]: "https://rpc.ankr.com/eth",
  [BALANCER_SUPPORTED_CHAIN_IDS.Polygon]: "https://rpc.ankr.com/polygon",
  [BALANCER_SUPPORTED_CHAIN_IDS.Arbitrum]: "https://rpc.ankr.com/arbitrum",
};

export const EXPLORERS: Record<number, string> = {
  [BALANCER_SUPPORTED_CHAIN_IDS.Ethereum]: "https://etherscan.io",
  [BALANCER_SUPPORTED_CHAIN_IDS.Polygon]: "https://polygonscan.com",
  [BALANCER_SUPPORTED_CHAIN_IDS.Arbitrum]: "https://arbiscan.io",
};
