import { TableColumnHeaderProps, Th } from '@chakra-ui/react'

export interface IBalTh extends TableColumnHeaderProps {}

export const BalTh = ({ children, ...props }: IBalTh) => (
  <Th
    fontFamily="cyborgSister"
    fontSize="lg"
    px={2}
    textAlign="center"
    {...props}
  >
    {children}
  </Th>
)
