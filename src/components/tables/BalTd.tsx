import { TableCellProps, Td } from '@chakra-ui/react'

export interface IBalTd extends TableCellProps {}

export const BalTd = ({ children, ...props }: IBalTd) => (
  <Td fontFamily="cyborgSister" fontSize="xl" px={3} {...props}>
    {children}
  </Td>
)
