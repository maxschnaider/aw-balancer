import { Text, TextProps } from '@chakra-ui/react'

export interface IBalText extends TextProps {}

export const BalText = ({ children, ...props }: IBalText) => (
  <Text fontSize="xl" letterSpacing="wide" {...props}>
    {children}
  </Text>
)
