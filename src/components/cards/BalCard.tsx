import { Box, BoxProps, StackItem, VStack } from '@chakra-ui/react';
import { ReactElement } from 'react';
import { BalText } from '@aw-balancer/components';

export interface IBalCard extends BoxProps {
  headerText?: string | undefined;
  header?: () => ReactElement;
}

export const BalCard = ({
  children,
  headerText,
  header,
  ...props
}: IBalCard) => (
  <Box borderRadius="lg" bg="balancer.bgLighter" p={4} {...props}>
    <VStack>
      <StackItem w="full">
        {!header ? (
          <BalText letterSpacing="wider" pb={3}>
            {headerText}
          </BalText>
        ) : (
          header()
        )}
      </StackItem>
      <StackItem w="full">{children}</StackItem>
    </VStack>
  </Box>
);
