import { ChevronDownIcon } from '@chakra-ui/icons';
import {
  Button,
  Menu,
  MenuButton,
  MenuButtonProps,
  MenuItem,
  MenuList,
} from '@chakra-ui/react';
import { BalMenuChain } from '@aw-balancer/components';

export interface IBalMenuButton extends MenuButtonProps {
  chains: BalMenuChain[];
  onChainSelect: (chainId: string | number) => void;
}

export const BalMenuButton = ({
  children,
  chains,
  onChainSelect,
  ...props
}: IBalMenuButton) => (
  <Menu>
    <MenuButton
      as={Button}
      rightIcon={<ChevronDownIcon />}
      fontSize="xl"
      fontWeight="normal"
      letterSpacing="wider"
      borderRadius="lg"
      {...props}
    >
      {children}
    </MenuButton>
    <MenuList bg="balancer.bg">
      {chains.map((chain) => (
        <MenuItem
          fontSize="lg"
          key={chain.id}
          onClick={() => onChainSelect(chain.id)}
        >
          {chain.name}
        </MenuItem>
      ))}
    </MenuList>
  </Menu>
);
