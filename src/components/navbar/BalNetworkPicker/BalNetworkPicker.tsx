import { useLogger } from '@aw-balancer/hooks';
import {
  BalancerSupportedChain,
  BALANCER_SUPPORTED_CHAIN_IDS,
} from '@aw-balancer/constants';
import { useNetwork, useSwitchNetwork } from 'wagmi';
import { BalMenuButton, IBalMenuButton } from '@aw-balancer/components';
import { useMemo } from 'react';

export type BalMenuChain = {
  id: string | number;
  name: string;
};

const chains: BalMenuChain[] = Object.keys(BALANCER_SUPPORTED_CHAIN_IDS).map(
  (chainName) => {
    const chainId =
      BALANCER_SUPPORTED_CHAIN_IDS[chainName as BalancerSupportedChain];
    return { id: chainId, name: chainName };
  }
);

export interface IBalNetworkPicker
  extends Omit<IBalMenuButton, 'children' | 'chains' | 'onChainSelect'> {}

export const BalNetworkPicker = ({ ...props }: IBalNetworkPicker) => {
  const log = useLogger(BalNetworkPicker.name);

  /**
   * network
   */
  const { chain: activeChain } = useNetwork();
  const { switchNetworkAsync } = useSwitchNetwork();

  const isSupportedChain = useMemo(
    () =>
      !!activeChain &&
      Object.values(BALANCER_SUPPORTED_CHAIN_IDS).includes(activeChain.id),
    [activeChain]
  );

  return (
    <BalMenuButton
      chains={chains}
      bg={isSupportedChain ? 'balancer.bgLighter' : 'red'}
      _hover={{ bg: isSupportedChain ? 'balancer.gradient' : 'red' }}
      _active={{ bg: isSupportedChain ? 'balancer.gradient' : 'red' }}
      onChainSelect={(chainId) => {
        log.verbose('ON_SELECT_MENU_ITEM', chainId);
        switchNetworkAsync?.(Number(chainId));
      }}
      {...props}
    >
      {isSupportedChain ? activeChain?.name : 'Switch Network'}
    </BalMenuButton>
  );
};
