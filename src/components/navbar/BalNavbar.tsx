import { HStack, Image, StackItem, StackProps } from '@chakra-ui/react';
import { BalancerLogo } from '@aw-balancer/assets';
import { BalButton, BalNetworkPicker, BalText } from '@aw-balancer/components';
import { useBalRouter, BalancerFiView } from '@aw-balancer/router';

export interface IBalNavbar extends Omit<StackProps, 'children'> {}

const tabs: BalancerFiView[] = [BalancerFiView.Invest, BalancerFiView.Trade];

export const BalNavbar = ({ ...props }: IBalNavbar) => {
  /**
   * routing
   */
  const [{ active: activeRoute }, { navigate }] = useBalRouter();

  /**
   * UI
   */
  const renderLogo = () => (
    <HStack spacing={3}>
      <StackItem>
        <Image src={BalancerLogo} h="40px" w="40px" p={1} />
      </StackItem>
      <StackItem display={['none', 'block']}>
        <BalText fontSize="3xl">Balancer</BalText>
      </StackItem>
    </HStack>
  );

  const renderTabs = () => (
    <HStack spacing={[3, 5]} h="full">
      {tabs.map((tab) => (
        <StackItem key={tab} h="full">
          <BalButton
            variant="unstyled"
            fontSize="2xl"
            h="95%"
            borderTop="5px solid"
            borderRadius="none"
            borderColor={
              activeRoute.path === tab ? 'balancer.blue' : 'transparent'
            }
            color={activeRoute.path === tab ? 'balancer.blue' : 'white'}
            _hover={{
              borderColor: activeRoute.path === tab ? '' : 'balancer.yellow',
              color: activeRoute.path === tab ? '' : 'balancer.yellow',
            }}
            onClick={() => navigate({ path: tab })}
          >
            {tab}
          </BalButton>
        </StackItem>
      ))}
    </HStack>
  );

  const renderNetworkPicker = () => <BalNetworkPicker />;

  return (
    <HStack
      px={4}
      justifyContent="space-between"
      alignItems="center"
      mb="-10px"
      spacing={[4, 8]}
      h="69px"
      {...props}
    >
      <StackItem>{renderLogo()}</StackItem>
      <StackItem flex={1} h="full">
        {renderTabs()}
      </StackItem>
      <StackItem>{renderNetworkPicker()}</StackItem>
    </HStack>
  );
};
