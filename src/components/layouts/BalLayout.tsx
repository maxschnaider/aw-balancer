import {
  ChakraProvider,
  StackItem,
  StackProps,
  useColorMode,
  VStack,
} from '@chakra-ui/react';
import { Styles } from '@aw-balancer/components';
import { useEffect } from 'react';
import { BalNavbar } from '@aw-balancer/components';
import { balancerTheme } from '@aw-balancer/theme';

export interface IBalLayout extends StackProps {}

export const BalLayout = ({ children, ...props }: IBalLayout) => {
  /**
   * UI
   */
  const { setColorMode } = useColorMode();
  useEffect(() => setColorMode('dark'), []);

  const renderNavbar = () => <BalNavbar />;

  return (
    <ChakraProvider theme={balancerTheme}>
      <Styles />
      <VStack
        h="full"
        w="full"
        fontFamily="cyborgSister"
        overflowY="auto"
        bg="balancer.bg"
        {...props}
      >
        <StackItem w="full">{renderNavbar()}</StackItem>
        <StackItem w="full">{children}</StackItem>
      </VStack>
    </ChakraProvider>
  );
};
