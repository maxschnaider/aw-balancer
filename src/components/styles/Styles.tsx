import {
  Fonts,
  FocusInvisibility,
  TransparentScrollbar,
} from '@aw-balancer/components';

export const Styles = () => (
  <>
    <Fonts />
    <FocusInvisibility />
    <TransparentScrollbar />
  </>
);
