import { Global } from '@emotion/react';

export const FocusInvisibility = () => (
  <Global
    styles={`
      *:focus {
        outline: none !important;
        box-shadow: none !important;
      }
    `}
  />
);
