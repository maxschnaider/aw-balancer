import {
  Flex,
  HStack,
  NumberInput,
  NumberInputField,
  Slider,
  SliderFilledTrack,
  SliderTrack,
  StackItem,
  StackProps,
  VStack,
} from '@chakra-ui/react';
import { BalTokenAmount } from '@aw-balancer/types';
import { useEffect, useMemo, useState } from 'react';
import { BalButton, BalText } from '@aw-balancer/components';
import { TokenAvatar } from '@aw-balancer/components';
import { FaEthereum } from 'react-icons/fa';

export interface IBalTokenAmountInput
  extends Omit<StackProps, 'children' | 'onChange'> {
  tokenAmount?: BalTokenAmount;
  balance?: string | undefined;
  tokenPriceUsd?: number | undefined;
  readonly?: boolean;
  onAmountChange?: (newAmount: string) => void | undefined;
  onTokenButtonClick?: () => void;
}

export const BalTokenAmountInput = ({
  tokenAmount,
  balance,
  tokenPriceUsd,
  readonly,
  onAmountChange,
  onTokenButtonClick,
  ...props
}: IBalTokenAmountInput) => {
  /**
   * balance
   */
  const isBalanceExists = useMemo(() => Number(balance) > 0, [balance]);

  /**
   * amount
   */
  const [amountUsd, setAmountUsd] = useState(0);

  useEffect(() => {
    if (!tokenPriceUsd || !tokenAmount?.amount) return setAmountUsd(0);
    setAmountUsd(Number(tokenAmount.amount) * tokenPriceUsd);
  }, [tokenAmount, tokenPriceUsd]);

  const isAmountExceedBalance = useMemo(
    () => !readonly && Number(tokenAmount?.amount) > Number(balance),
    [balance, tokenAmount]
  );

  /**
   * handles
   */
  const handleMaxButtonClick = () => onAmountChange?.(balance || '0');

  /**
   * slider
   */
  const [sliderValue, setSliderValue] = useState<number>(0);
  useEffect(() => {
    if (!balance || !tokenAmount?.amount) return setSliderValue(0);
    const newSliderValue =
      (Number(tokenAmount?.amount) / Number(balance)) * 100;
    setSliderValue(newSliderValue);
  }, [tokenAmount, balance]);

  /**
   * UI
   */
  const renderTokenButton = () => (
    <BalButton minW="contents" px={[2, 4]} onClick={onTokenButtonClick}>
      <HStack justifyContent="space-evenly" w="full">
        <StackItem>
          {!!tokenAmount ? (
            <TokenAvatar
              size="xs"
              logoURI={tokenAmount?.token.logoURI}
              address={tokenAmount?.token.address}
              name={tokenAmount?.token.symbol}
            />
          ) : (
            <FaEthereum fontSize="16px" />
          )}
        </StackItem>
        <StackItem>
          <BalText>
            {!!tokenAmount ? tokenAmount.token.symbol : 'Select token'}
          </BalText>
        </StackItem>
      </HStack>
    </BalButton>
  );

  const renderInput = () => (
    <NumberInput
      variant="unstyled"
      size="lg"
      lineHeight={10}
      min={0}
      value={tokenAmount?.amount}
      onChange={onAmountChange}
      pointerEvents={readonly || !tokenAmount ? 'none' : 'auto'}
    >
      <NumberInputField
        p={0}
        textAlign="right"
        fontSize="3xl"
        color={isAmountExceedBalance ? 'red.400' : 'white'}
        placeholder="0.0"
      />
    </NumberInput>
  );

  const renderBalance = () => (
    <BalText opacity="0.5" fontSize="lg">
      Balance: {balance?.substring(0, 6) || '0'}
    </BalText>
  );

  const renderMaxButton = () => (
    <BalButton
      variant="unstyled"
      fontSize="md"
      h="full"
      color="balancer.blue"
      onClick={handleMaxButtonClick}
    >
      Max
    </BalButton>
  );

  const renderAmountUsd = () => (
    <BalText opacity="0.5" textAlign="right" fontSize="lg" lineHeight={0}>
      ${amountUsd.toFixed(2)}
    </BalText>
  );
  const renderSlider = () => (
    <Slider aria-label="slider-ex-1" value={sliderValue} pointerEvents="none">
      <SliderTrack bg="whiteAlpha.200">
        <SliderFilledTrack
          bg={isAmountExceedBalance ? 'red.400' : 'balancer.gradient'}
        />
      </SliderTrack>
    </Slider>
  );

  const renderExceedsBalanceAlert = () => (
    <BalText fontSize="lg" color="red.400" pt={2}>
      Exceeds wallet balance
    </BalText>
  );

  return (
    <VStack
      spacing={1}
      justifyContent="space-between"
      bg="blackAlpha.100"
      borderRadius="lg"
      borderWidth="1px"
      borderColor={isAmountExceedBalance ? 'red.400' : 'blackAlpha.500'}
      _hover={{ borderColor: 'whiteAlpha.200' }}
      transition=".2s"
      px={3}
      pt={3}
      pb={2}
      {...props}
    >
      <StackItem w="full">
        <Flex gap={3} justifyContent="space-between">
          {renderTokenButton()}
          {renderInput()}
        </Flex>
      </StackItem>
      <StackItem w="full">
        <VStack spacing={-3}>
          <StackItem w="full">
            <HStack justifyContent="space-between">
              <StackItem>
                <HStack spacing={0}>
                  {!!tokenAmount && <StackItem>{renderBalance()}</StackItem>}
                  {isBalanceExists && !readonly && (
                    <StackItem>{renderMaxButton()}</StackItem>
                  )}
                </HStack>
              </StackItem>
              {amountUsd > 0 && <StackItem>{renderAmountUsd()}</StackItem>}
            </HStack>
          </StackItem>
          {isBalanceExists && !readonly && (
            <StackItem w="full">{renderSlider()}</StackItem>
          )}
          {isAmountExceedBalance && (
            <StackItem w="full">{renderExceedsBalanceAlert()}</StackItem>
          )}
        </VStack>
      </StackItem>
    </VStack>
  );
};
