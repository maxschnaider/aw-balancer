import { CloseIcon } from '@chakra-ui/icons';
import {
  IconButton,
  Modal,
  ModalBody,
  ModalBodyProps,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  ModalProps,
} from '@chakra-ui/react';
import { BalText } from '@aw-balancer/components';

export type IBalModal = ModalProps &
  ModalBodyProps & {
    title?: string | undefined;
  };

export const BalModal = ({
  title,
  onClose,
  isOpen,
  children,
  ...props
}: IBalModal) => (
  <Modal onClose={onClose} isOpen={isOpen} isCentered>
    <ModalOverlay
      bgBlendMode="soft-light,soft-light,normal"
      bg="balancer.modalOverlayGradient"
    />
    <ModalContent
      fontFamily="cyborgSister"
      bg={props.bg || 'balancer.bg'}
      borderRadius="xl"
      mx={4}
    >
      <ModalHeader display="flex" justifyContent="space-between">
        <BalText fontSize="2xl" fontWeight="normal">
          {title}
        </BalText>

        <IconButton
          onClick={onClose}
          aria-label="CLOSE_BUTTON"
          icon={<CloseIcon />}
          size="sm"
          fontSize="xs"
          variant="ghost"
        />
      </ModalHeader>
      <ModalBody p={0} {...props}>
        {children}
      </ModalBody>
      {/* <ModalFooter /> */}
    </ModalContent>
  </Modal>
);
