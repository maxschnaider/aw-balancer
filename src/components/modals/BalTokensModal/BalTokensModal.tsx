import {
  HStack,
  Input,
  ModalProps,
  ModalBodyProps,
  Skeleton,
  SkeletonCircle,
  StackDivider,
  StackItem,
  VStack,
} from '@chakra-ui/react';
import { useTokenBalances, useTokensPriceUsd } from '@aw-balancer/hooks';
import { BalToken } from '@aw-balancer/types';
import { ChangeEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { BalModal, BalText } from '@aw-balancer/components';
import Fuse from 'fuse.js';
import { useBalTokenList } from '@aw-balancer/hooks';
import { TokenAvatar } from '@aw-balancer/components';
import { FetchBalanceResult } from '@wagmi/core';

export type IBalTokensModal = Omit<ModalProps, 'children'> &
  ModalBodyProps & {
    onTokenSelect: (token: BalToken) => void;
  };

export const BalTokensModal = ({
  onClose,
  isOpen,
  onTokenSelect,
  ...props
}: IBalTokensModal) => {
  /**
   * tokens
   */
  const { data: tokens, isLoading: tokenListQueryLoading } = useBalTokenList();

  /**
   * balances
   */
  const { data: balances, isLoading: isBalancesLoading } = useTokenBalances({
    addresses: tokens?.map((token) => token.address) || [],
  });

  const { data: tokenPriceUsdByAddress, isLoading: tokensPriceUsdLoading } =
    useTokensPriceUsd({
      addresses: tokens?.map((token) => token.address) || [],
    });

  const balanceUsdByAddress = useMemo(() => {
    const _balanceUsdByAddress: Record<string, number> = {};
    balances?.forEach((balance) => {
      const tokenPriceUsd =
        tokenPriceUsdByAddress && tokenPriceUsdByAddress[balance.address];
      const balanceUsd = !!tokenPriceUsd
        ? tokenPriceUsd * Number(balance.formatted)
        : 0;
      _balanceUsdByAddress[balance.address] = balanceUsd;
    });
    return _balanceUsdByAddress;
  }, [balances, tokenPriceUsdByAddress]);

  /**
   * search & filter
   */
  const [searchQuery, setSearchQuery] = useState<string | undefined>();

  useEffect(() => {
    setSearchQuery(undefined);
  }, [isOpen]);

  const filteredTokenList = useMemo(() => {
    if (!tokens) return [];
    let tokenList = [...tokens];
    if (!!searchQuery) {
      const fuse = new Fuse(tokens, {
        keys: ['symbol', 'address', 'name'],
        isCaseSensitive: false,
      });
      const result = fuse.search(searchQuery);
      tokenList = result.map((d) => d.item);
    }
    if (Object.entries(balanceUsdByAddress).length > 0) {
      tokenList = tokenList.sort((tokenA, tokenB) => {
        const tokenABalanceUsd = balanceUsdByAddress[tokenA.address] || 0;
        const tokenBBalanceUsd = balanceUsdByAddress[tokenB.address] || 0;
        return (
          tokenABalanceUsd === 0 && tokenBBalanceUsd === 0
            ? tokenA.symbol.toLowerCase() > tokenB.symbol.toLowerCase()
            : tokenABalanceUsd < tokenBBalanceUsd
        )
          ? 1
          : -1;
      });
    }
    return tokenList;
  }, [tokens, searchQuery, balanceUsdByAddress]);

  const handleSearchQueryChange = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      setSearchQuery(event.target.value);
    },
    []
  );

  /**
   * UI
   */
  const renderSearchInput = () => (
    <Input
      variant="ghost"
      bg="transparent"
      fontSize="2xl"
      fontWeight="normal"
      px={6}
      placeholder="Name, symbol or address"
      onChange={handleSearchQueryChange}
      value={searchQuery}
    />
  );

  const renderSkeletonTokenList = () => (
    <VStack overflowY="auto" maxH="60vh" spacing={0}>
      {Array.from({ length: 7 }).map((_, index) => (
        <StackItem key={index} w="full" h="60px" px={4} py={2}>
          <HStack spacing={4}>
            <StackItem>
              <SkeletonCircle w="48px" h="48px" />
            </StackItem>
            <VStack flex={1} spacing={2} alignItems="flex-start">
              <Skeleton h="15px" w="120px" />
              <Skeleton h="15px" w="120px" />
            </VStack>
            <VStack textAlign="right" spacing={2} alignItems="flex-end">
              <Skeleton h="15px" w="60px" />
              <Skeleton h="15px" w="60px" />
            </VStack>
          </HStack>
        </StackItem>
      ))}
    </VStack>
  );

  const renderTokenListItem = (
    token: BalToken,
    balanceUsd: number,
    balance?: FetchBalanceResult
  ) => (
    <HStack
      spacing={4}
      cursor="pointer"
      maxW="full"
      px={4}
      py={3}
      justifyContent="space-between"
      _hover={{ bg: 'blackAlpha.200' }}
      onClick={() => onTokenSelect(token)}
    >
      <StackItem>
        <TokenAvatar
          logoURI={token.logoURI}
          address={token.address}
          name={token.symbol}
        />
      </StackItem>
      <StackItem maxW="150px">
        <VStack alignItems="start" spacing={1}>
          <StackItem>
            <BalText fontSize="2xl" lineHeight={6}>
              {token.symbol}
            </BalText>
          </StackItem>
          <StackItem>
            <BalText opacity="0.5" lineHeight={4}>
              {token.name}
            </BalText>
          </StackItem>
        </VStack>
      </StackItem>
      <StackItem flex={1}>
        <VStack textAlign="right" spacing={1} alignItems="end">
          <StackItem>
            <BalText fontSize="2xl" lineHeight={6}>
              {isBalancesLoading ? (
                <Skeleton h="20px" w="120px" />
              ) : (
                balance?.formatted?.substring(0, 6) || '0.00'
              )}
            </BalText>
          </StackItem>
          <StackItem>
            <BalText opacity="0.5" lineHeight={4}>
              {tokensPriceUsdLoading || isBalancesLoading ? (
                <Skeleton h="15px" w="60px" />
              ) : (
                `$${balanceUsd.toFixed(2)}`
              )}
            </BalText>
          </StackItem>
        </VStack>
      </StackItem>
    </HStack>
  );

  const renderTokenList = () => (
    <VStack overflowY="auto" maxH="60vh" spacing={0}>
      {filteredTokenList.map((token) => {
        const balance = balances?.find(
          (balance) => balance.address === token.address
        );
        const balanceUsd = balanceUsdByAddress[token.address] || 0;
        return (
          <StackItem key={token.address} w="full">
            {renderTokenListItem(token, balanceUsd, balance)}
          </StackItem>
        );
      })}
    </VStack>
  );
  return (
    <BalModal
      title="Token search"
      isOpen={isOpen}
      onClose={onClose}
      minH="40vh"
      {...props}
    >
      <VStack divider={<StackDivider />}>
        <StackItem w="full">
          {tokenListQueryLoading ? (
            <Skeleton h="40px" w="full" />
          ) : (
            renderSearchInput()
          )}
        </StackItem>
        <StackItem w="full">
          {tokenListQueryLoading
            ? renderSkeletonTokenList()
            : renderTokenList()}
        </StackItem>
      </VStack>
    </BalModal>
  );
};
