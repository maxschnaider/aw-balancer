import { Button, ButtonProps } from '@chakra-ui/react'

export interface IBalButton extends ButtonProps {}

export const BalButton = ({ children, ...props }: IBalButton) => (
  <Button fontSize="xl" fontWeight="normal" borderRadius="lg" {...props}>
    {children}
  </Button>
)
