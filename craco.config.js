const path = require(`path`);

module.exports = {
  webpack: {
    alias: {
      '@aw-balancer': path.resolve(__dirname, 'src/'),
    },
  },
};
